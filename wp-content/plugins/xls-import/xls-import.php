<?php

/*
Plugin Name: Icebug XLS Import
Plugin URI: http://www.raketwebbyra.se/
Description:
Version: 1.0.0
Author: Edvin Brobeck
Author URI: http://www.raketwebbyra.se/
License: GPL
Copyright: Edvin Brobeck
*/


/*
 *
 *
   SCHEMA for XLS data 1/10 2013
        (
            [A] => STORE NAME
            [B] => STREET
            [C] => ZIP AND CITY
            [D] => COUNTRY
            [E] => PHONE
            [F] => EMAIL
            [G] => WEBSITE
        )


 *
 *
 */


if (is_admin()) {
    add_action('admin_menu', 'add_admin_menues');
    add_action('admin_init', 'xlsimport_register_settings');
}


// Google Maps V3 API
if(!class_exists('geocoder')) {
    class geocoder {
        static private $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=";

        static public function getLocation($address){
            $url = self::$url.urlencode($address);

            $resp_json = self::curl_file_get_contents($url);
            $resp = json_decode($resp_json, true);

            if($resp['status']='OK'){
                return $resp['results'][0]['geometry']['location'];
            }else{
                return false;
            }
        }

        static private function curl_file_get_contents($URL){
            $c = curl_init();
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($c, CURLOPT_URL, $URL);
            $contents = curl_exec($c);
            curl_close($c);

            if ($contents) return $contents;
            else return FALSE;
        }
    }
}
function xls_geocode_address($address) {
    //$address = get_field('retailer_post_address') . ' ' . get_field('retail_address');
    $geolocation = geocoder::getLocation($address);
    return $geolocation;
}

// Read retailers file
function read_xls_to_array($path = false) {
    define('XLSIMPORTPLUGIN_PATH', dirname(__FILE__));
    require_once ( XLSIMPORTPLUGIN_PATH . '/lib/Classes/PHPExcel.php');


    if(!$path) {
        $path = get_attached_file(get_field('retailers_import_file','options'));
    }

    /* Check if files exists and if it's a correct XLS file */
    if ( !file_exists($path) ) {
        echo '.xls Filen verkar inte finnas.';
        return false;
    }

    if ( substr($path, -3) != 'xls' ) {
        echo 'Fel filformat på importfilen. Det måste vara en .xls fil';
        return false;
    }

    // Initiate
    $objPHPExcel = new PHPExcel();

    // Load file and set Read only
    $objReader = new PHPExcel_Reader_Excel5();
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load( $path );


    // Initiate rowIterator
    $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();

    $array_data = array();
    foreach($rowIterator as $row){
        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
        if(1 == $row->getRowIndex ()) continue;//skip first row
        $rowIndex = $row->getRowIndex ();
        $array_data[$rowIndex] = array('A'=>'', 'B'=>'','C'=>'','D'=>'', 'E' => '', 'F' => '', 'G' => '');

        foreach ($cellIterator as $cell) {
            if('A' == $cell->getColumn()){
                $array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
            } else if('B' == $cell->getColumn()){
                $array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
            } else if('C' == $cell->getColumn()){
                $array_data[$rowIndex][$cell->getColumn()] = PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), 'YYYY-MM-DD');
            } else if('D' == $cell->getColumn()){
                $array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
            } else if('E' == $cell->getColumn()){
                $array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
            } else if('F' == $cell->getColumn()){
                $array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
            } else if('G' == $cell->getColumn()){
                $array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
            }
        }
    }


    return $array_data;
}

// Get array of current retailers
function xls_get_current_retailers(){
    $retailers_posts = get_posts(array( 'post_type' => 'retailer', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC', 'post_status' => 'any' ));
    return $retailers_posts;
}

// sanitize match string
function sanitize_match_string($str) {
    mb_str_replace(
        array('(tom)', ' '), // Search
        array('',''), // Replace
        $str
    );
    return $str;
}

// Add menu item
function add_admin_menues() {
    add_management_page( 'Importera ÅF', 'Importera ÅF', 'manage_options', 'sync_retailers', 'sync_retailers' );
}



// Make view
function sync_retailers() {
    if (!current_user_can('edit_pages'))  {
        wp_die( __('You do not have sufficient permissions to access this page.') );
    }
?>

    <div class="wrap">

        <div class="icon32" id="icon-options-general"></div>

        <h2>Importera återförsäljare från XLS fil</h2>

        <form method="post" action="options.php" enctype="multipart/form-data">
            <?php settings_fields( 'xlsimport-settings-group' ); ?>

            <table class="form-table">

                <tr valign="top">
                    <th scope="row">Importfil</th>
                    <td>
                        <label for="xlsimport_setting_2">
                            <input name="xlsimport_setting_3" type="text" id="xlsimport_setting_3" value="<?php echo get_field('retailers_import_file','options') ?>" disabled="disabled" /><br />
                            <strong>Filen som kommer att användas vid importen laddar du upp under "Options > Återförsäljare > Källfil".</strong>
                        </label>
                    </td>
                </tr>
            </table>

            <p class="submit">
                <input type="submit" name="" class="button-primary" value="<?php _e('Importera återförsäljare') ?>" />
            </p>
        </form>

<?php

    // if submited, lets go import dat shieat.
    if($_GET['settings-updated'] == 'true' && $_GET['page'] == 'sync_retailers'){

        $path = get_field('retailers_import_file','options');
        $path = get_attached_file( $path );

        // Load up XLS file to php array
        $current_retailers = xls_get_current_retailers();
        $new_retailers = read_xls_to_array($path);

        /* If the xls file import is successful */
        if ( !$new_retailers ) return;

        /* Remove current retailers if there are any */
        if ( count ($current_retailers) > 0 ) {
            foreach ( $current_retailers as $curr_ret ) {
                wp_delete_post($curr_ret->ID, true);
            }
        }

        /* Create the new retailers in the db */
        foreach ( $new_retailers as $r ) {

            $args = array(
                'post_title' => $r['A'],
                'post_type' => 'retailer',
                'post_status' => 'publish'
            );
            $new_retailer_id = wp_insert_post($args, true);

            // If value of column is "(tom)" - assume it's empty
            $street = ( $r['B'] != '(tom)' ) ? $r['B'] : '';
            $zip_and_city = ( $r['C'] != '(tom)' ) ? $r['C'] : '';
            $phone = ( $r['E'] != '(tom)' ) ? $r['E'] : '';
            $email = ( $r['F'] != '(tom)' ) ? $r['F'] : '';
            $homepage = ( $r['G'] != '(tom)' ) ? $r['G'] : '';


            // Field Keys
            $street_field_key = 'field_5204a18e2df5b';
            $zip_city_field_key = 'field_5204a2181f1c5';
            $phone_field_key = 'field_5204a1722df59';
            $email_field_key = 'field_5204a1852df5a';
            $homepage_field_key = 'field_524543ab28054';

            // Update Custom Fields
            if ( $street ) update_field($street_field_key, $street, $new_retailer_id);
            if ( $zip_and_city ) update_field($zip_city_field_key, $zip_and_city, $new_retailer_id);
            if ( $phone ) update_field($phone_field_key, $phone, $new_retailer_id);
            if ( $email ) update_field($email_field_key, $email, $new_retailer_id);
            if ( $homepage ) update_field($homepage_field_key, $homepage, $new_retailer_id);

            $address = $street . ' ' . $zip_and_city;

            /* Ask Google API V3 for Geocoding Lat and Lng */
            $geocoding_object = geocoder::getLocation($address);

            /* If it succeeds go ahead and save the lat and long to field */
            if ( $geocoding_object ) {
                $lat_lng = $geocoding_object['lat'] . ',' . $geocoding_object['lng'];
                $address_lat_lng = $r['B'] . ' ' . $r['C'] . '|' . $lat_lng;
                update_post_meta($new_retailer_id, 'retailer_geolocation', $address_lat_lng);
            } else {
                /* If not then put the post in draft */
               $my_post = array(
                   'ID' => $new_retailer_id,
                   'post_status' => 'draft'
               );
               wp_update_post($my_post);
            }

        }
    }
    // Close wrap for WP layout
    echo '</div>';
}


// Save settings
function xlsimport_register_settings() {
    register_setting( 'xlsimport-settings-group', 'xlsimport_setting_1' );
    register_setting( 'xlsimport-settings-group', 'xlsimport_setting_2' );
}