<?php get_header(); ?>
<?php get_template_part('/template-parts/mobile/mobile-sidebar'); ?>
<?php get_template_part('/template-parts/breadcrumbs'); ?>
    <section id="main-section" role="main">
        <div class="container">
            <div class="row">

                <div id="content" class="span8">

                    <h2 class="page-title">Sökresultat för: "<?php echo get_search_query(); ?>"</h2>
                    <?php
                    global $wp_query;
                    if( $wp_query->found_posts > 0 ) : ?>

                    <div class="posts">
                        <?php get_template_part('template-parts/loops/loop', 'index'); ?>
                    </div>

                    <?php get_template_part('template-parts/pagination'); ?>

                    <?php else :  ?>

                        <p>Vi kunde inte hitta något som matchar din sökning.</p>

                    <?php endif; ?>

                </div>

            </div>
        </div><!--//container-->
    </section>
<?php get_footer(); ?>