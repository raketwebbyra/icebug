<div id="ppl"></div>

<!-- Footer -->
<footer id="footer-section" class="module" role="contentinfo" data-base-url="<?php echo bloginfo('home') ?>">
    <div class="container footer-wrap">
        <div class="row">
            <div class="span8 mobile-module">

                <div class="row">
                    <div class="span4">
                        <h5 class="title"><?php _ex('Herrskor','Sidfot','icebug') ?></h5>
                        <ul class="list-unstyled">
                            <?php
                                $group_term = 3;  // Herrar
                                $gender = icl_object_id($group_term,'product_group');
                                $gender = get_term($gender, 'product_group');
                                $tax = 'product_category';
                                $terms = get_terms($tax);
                                foreach($terms as $term):
                            ?>
                            <li><a href="<?php echo get_post_type_archive_link('product') ?>?product_group=<?php echo $gender->slug ?>&amp;product_category=<?php echo $term->slug ?>" title=""><?php echo $term->name ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="span4">
                        <h5 class="title"><?php _ex('Damskor','Sidfot','icebug') ?></h5>
                        <ul class="list-unstyled">
                            <?php
                                $group_term = 4;  // Damer
                                $gender = icl_object_id($group_term,'product_group');
                                $gender = get_term($gender, 'product_group');
                                $tax = 'product_category';
                                $terms = get_terms($tax);
                                foreach($terms as $term):
                            ?>
                                <li><a href="<?php echo get_post_type_archive_link('product') ?>?product_group=<?php echo $gender->slug ?>&amp;product_category=<?php echo $term->slug ?>" title=""><?php echo $term->name ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="span4">
                        <h5 class="title"><?php _ex('Grepp & Tech','Sidfot','icebug') ?></h5>
                        <ul class="list-unstyled">
                            <?php
                                wp_nav_menu( array(
                                    'theme_location' => 'footer_navigation',
                                    'items_wrap' => '%3$s'
                                ) );
                            ?>
                        </ul>
                    </div>
                </div>

            </div>

            <div id="newsletter" class="span4">
                <h5 class="title"><?php _ex('Prenumerera på nyhetsbrev','Nyhetsbrev','icebug') ?></h5>

                <form id="newsletter-form" action="<?php echo bloginfo('home'); ?>">
                    <div class="input-group col-lg-7">
                        <input type="text"  name="email" id="nl-email" placeholder="<?php _ex('Din e-postadress','Nyhetsbrev','icebug'); ?>">
                        <input type="hidden" name="country" id="nl-country" />
                          <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><?php _ex('OK','Nyhetsbrev','icebug') ?></button>
                          </span>
                    </div>
                </form>

                <h5 class="title"><?php _ex('Icebug på Facebook','Nyhetsbrev','icebug') ?></h5>

                <div <?php if ( is_tablet() || is_mobile() ) echo 'layout="box_count"' ?> class="fb-like" data-href="https://www.facebook.com/Icebug.Official" data-send="false" data-width="250" data-show-faces="false" data-colorscheme="light"></div>

                <div class="social-icons">
                    <a class="social-icon facebook-social-btn" target="_blank" href="https://www.facebook.com/Icebug.Official"></a>
                    <a class="social-icon twitter-social-btn" target="_blank" href="https://twitter.com/IcebugOf"></a>
                    <a class="social-icon instagram-social-btn" target="_blank" href="http://instagram.com/icebugof"></a>
                </div>

            </div>
        </div>
    </div>
</footer>

<!-- End -->
<section id="end-section" class="module" role="contentinfo">
    <div class="container">
    <div class="row">
        <div class="span12">

        <ul class="bottom-menu list-inline">
            <li>
                <a href="" title="">
                    <img src="<?php bloginfo("template_url"); ?>/images/emblem.png" width="30" height="30" alt=""/>
                </a>
            </li>
            <li><?php echo get_field_by_language('contact_name', 'option') ?></li>
            <li><?php echo get_field_by_language('contact_street_address', 'option') ?></li>
            <li><?php echo get_field_by_language('contact_post_address', 'option') ?></li>
            <li>Tel: <?php echo get_field_by_language('contact_phone', 'option') ?></li>
            <li>Fax: <?php echo get_field_by_language('contact_fax', 'option') ?></li>
            <li><a href="mailto:<?php echo get_field_by_language('contact_email', 'option') ?>" title=""><?php echo get_field_by_language('contact_email', 'option') ?></a></li>
        </ul>

        </div>
    </div>
    </div>
</section>

</div><!--//page-wrap-->

<!-- Google Remarketing Tag -->
<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 979401387;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
	<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/979401387/?value=0&amp;guid=ON&amp;script=0"/>
	</div>
</noscript>

<!-- Scripts / WP-footer -->
<?php wp_footer(); ?>
<?php get_template_part('template-parts/modal'); ?>

</body>
</html>
