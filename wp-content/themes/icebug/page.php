<?php

if ( isset( $write_testimonial ) ) {
    require_once(get_stylesheet_directory().'/template-parts/create-new-testimonial.php');
}

get_header(); ?>

<!-- Breadcrumbs -->
<?php get_template_part('/template-parts/breadcrumbs'); ?>
<?php get_template_part('/template-parts/mobile/mobile-sidebar'); ?>

<section id="main-section" role="main">
    <div class="container">
        <div class="row">

            <aside id="sidebar" class="span3 hide-for-mobile">

                <?php
                // Output sub-menu
                echo '<ul class="sub-menu">';
                wp_list_pages(array(
                    'title_li' => '',
                    'link_before' => '<i class="icon icon-chevron-sign-down down"></i><i class="icon icon-chevron-sign-right"></i>',
                    'child_of' => ($ancestor = array_pop(get_post_ancestors($post->ID))) ? $ancestor : $post->ID
                ));
                echo '</ul>';

                // Ad loop
                include(get_stylesheet_directory().'/template-parts/ad-loop.php'); ?>

            </aside>

            <div id="page-content" class="span9" <?php live_edit('post_title,post_content,related_products') ?>>
                <?php while (have_posts()) : the_post(); ?>
                    <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
                        
                        <?php get_template_part('template-parts/page-slider'); ?>

                        <h1 class="page-title"><?php the_title(); ?></h1>
                        <div class="content"><?php the_content(); ?></div>
                        <?php
                            //  Should we list child pages?
                            if($list_child_pages) include(get_stylesheet_directory().'/template-parts/page-child-loop.php');

                            if($list_testimonials) include(get_stylesheet_directory().'/template-parts/testimonials-loop.php');

                            if($write_testimonial) include(get_stylesheet_directory().'/template-parts/write-testimonial.php');

                            if($contact_page) include(get_stylesheet_directory().'/template-parts/contact-form.php');

                            //  Do we have related products to this page?
                            if(get_field('related_products')) include(get_stylesheet_directory().'/template-parts/related-product-loop.php');

                            //  Else. Everbody Conga!

                        ?>
                    </article>
                <?php endwhile; //End the loop ?>
            </div>

        </div>
    </div>
</section>
<?php get_footer(); ?>