<?php get_header(); ?>
    <div <?php live_edit('modules') ?>>
 <?php
    if(get_field('modules')):
        $module_index = 0;
        while(has_sub_field('modules')):
            $module = get_row_layout();
            $file = get_stylesheet_directory().'/template-parts/modules/'.$module.'.php';
            if(file_exists($file)){
                include($file);
            } else {
                echo _x('Ingen mallfil hittades, kontakta utvecklare','Layoutbyggaren','icebug');
            }
            $module_index++;
        endwhile;
    else:
        the_content();
    endif;
?>
</div>
<?php get_footer(); ?>