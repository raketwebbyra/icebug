<?php get_header(); ?>


<!-- Breadcrumbs -->
<?php get_template_part('/template-parts/breadcrumbs');
$allProducts = fetch_all_products();
$productTypeTerms = get_term_list('product_type');
$productGroupTerms = get_term_list('product_group');
$productCategoryTerms = get_term_list('product_category');
$productTechGripTerms = get_term_image_list('product_tech_grip');
$productTechMembraneTerms = get_term_image_list('product_tech_membrane');
$productColdTerms = get_term_image_list('product_tech_cold'); ?>

<script type="text/javascript">
	window.allProducts = JSON.parse(JSON.stringify(<?php echo json_encode(fetch_all_products()); ?>));
	window.productTypeTerms = JSON.parse(JSON.stringify(<?php echo json_encode(get_term_list('product_type')); ?>));
	window.productGroupTerms = JSON.parse(JSON.stringify(<?php echo json_encode(get_term_list('product_group')); ?>));
	window.productCategoryTerms = JSON.parse(JSON.stringify(<?php echo json_encode(get_term_list('product_category')); ?>));
	window.productTechGripTerms = JSON.parse(JSON.stringify(<?php echo json_encode(get_term_image_list('product_tech_grip')); ?>));
	window.productTechMembraneTerms = JSON.parse(JSON.stringify(<?php echo json_encode(get_term_image_list('product_tech_membrane')); ?>));
	window.productColdTerms = JSON.parse(JSON.stringify(<?php echo json_encode(get_term_image_list('product_tech_cold')); ?>));
</script>
<div id="filtering" ng-app="FilteringApp" ng-controller="MainCtrl">

	<!-- Banner -->
	<section id="banner-section" class="module">
	<?php
	    if(get_field('banner_product_settings','option')):
	        if(get_field('banner_product_link_settings','option')) {
	            echo '<a href="'.get_field('banner_product_link_settings','option').'" title="Banner"><img src="'.get_field('banner_product_settings','option').'" alt=""/></a>';
	        } else {
	            echo '<img src="'.get_field('banner_product_settings','option').'" alt=""/>';
	        }
	    endif;
	?>
	</section>

	<section id="filter-section" ng-cloak class="module hide-for-no-js">
	    <div class="container">

	        <h5 ng-click="toggleMobileMenu()" class="title filter-btn show-for-tablet"><?php echo _x('Filtrera','Produktkatalog','icebug') ?> <i class="icon icon-cog"></i></h5>

	        <div class="products-cat-wrap" ng-class="{ menuOpen: mobileMenuOpen }">

		        <div class="row products-cat-links">
			        <div class="free-text-search">
				        <input placeholder="<?php echo _x('Produktnamn','Produktkatalog','icebug') ?>" ng-change="textFilter()" type="text" ng-model="textSearch"/>
			        </div>
		        </div>

	            <div class="row products-cat-links">
	                <div class="span3 products-cat-col rows-above">
	                    <div class="product-cat">
	                        <h5 ng-click="toggleTaxonomy('typeTaxonomyOpen')" class="title"><?php echo _x('Bäst för','Produktkatalog','icebug') ?></h5>
	                        <div class="product-cat-list-wrap" ng-class="{ taxOpen: typeTaxonomyOpen }">
	                            <ul>
	                                <li ng-repeat="term in productTypeTerms">
                                        <?php get_template_part('template-parts/filtering/term'); ?>
	                                </li>
	                            </ul>
	                        </div><!--//product-cat-list-wrap-->
	                    </div><!--//product-cat-->

	                    <div class="clearfix"></div>

	                    <div class="product-cat">
	                        <h5 ng-click="toggleTaxonomy('groupTaxonomyOpen')" class="title"><?php echo _x('Typ','Produktkatalog','icebug') ?></h5>
	                        <div class="product-cat-list-wrap" ng-class="{ taxOpen: groupTaxonomyOpen }">
	                            <ul>
		                            <li ng-repeat="term in productGroupTerms">
                                        <?php get_template_part('template-parts/filtering/term'); ?>
		                            </li>
	                            </ul>
	                        </div><!--//product-cat-list-wrap-->
	                    </div><!--//product-cat-->

	                </div>
	                <div class="span9 products-cat-col rows-above">
	                    <div class="row">
	                    <div class="product-cat">
	                        <h5 ng-click="toggleTaxonomy('categoryTaxonomyOpen')" class="title"><?php echo _x('Kategori','Produktkatalog','icebug') ?></h5>
	                        <div class="product-cat-list-wrap" ng-class="{ taxOpen: categoryTaxonomyOpen }">
	                            <ul>
		                            <li ng-repeat="term in productCategoryTerms">
                                        <?php get_template_part('template-parts/filtering/term'); ?>
		                            </li>
	                            </ul>
	                        </div><!--//product-cat-list-wrap-->
	                    </div><!--//product-cat-->

	                   </div>

	                    <div class="row product-cat-images-row">
	                        <div class="span3 product-cat product-cat-image">
	                            <h5 ng-click="toggleTaxonomy('gripTaxonomyOpen')" class="title"><?php echo _x('Grepp','Produktkatalog','icebug') ?></h5>
	                            <div class="product-cat-list-wrap" ng-class="{ taxOpen: gripTaxonomyOpen }">
	                                <ul>
		                                <li ng-repeat="term in productTechGripTerms">
			                                <?php get_template_part('template-parts/filtering/imageTerm'); ?>
		                                </li>
	                                </ul>
	                            </div><!--//product-cat-list-wrap-->
	                        </div>
	                        <div class="span3 product-cat product-cat-image product-cat-membrane">
	                            <h5 ng-click="toggleTaxonomy('membraneTaxonomyOpen')" class="title"><?php echo _x('Membran','Produktkatalog','icebug') ?></h5>
	                            <div class="product-cat-list-wrap" ng-class="{ taxOpen: membraneTaxonomyOpen }">
	                                <ul>
		                                <li ng-repeat="term in productTechMembraneTerms">
                                            <?php get_template_part('template-parts/filtering/imageTerm'); ?>
		                                </li>
	                                </ul>
	                            </div><!--//product-cat-list-wrap-->
	                        </div>
	                        <div class="span4 product-cat product-cat-image">
	                            <h5 ng-click="toggleTaxonomy('coldTaxonomyOpen')" class="title"><?php echo _x('Kyla','Produktkatalog','icebug') ?></h5>
	                            <div class="product-cat-list-wrap" ng-class="{ taxOpen: coldTaxonomyOpen }">
	                                <ul>
		                                <li ng-repeat="term in productColdTerms">
                                            <?php get_template_part('template-parts/filtering/imageTerm'); ?>
		                                </li>
	                                </ul>
	                            </div><!--//product-cat-list-wrap-->
	                        </div>
	                        <div class="span3 product-cat product-cat-image hide-for-tablet">
	                            <div class="product-cat-list-wrap">
	                                <a ng-click="resetFilter()" class="clean-all-filters btn filter-btn mobile-btn" data-href="<?php echo get_post_type_archive_link('product'); ?>" title="Rensa filtrering"><?php _ex('Återställ sökning', 'Produktkatalog', 'icebug') ?><i class="icon-reply"></i></a>
	                            </div><!--//product-cat-list-wrap-->
	                        </div>
	                    </div>
	                </div>
	                <a ng-click="resetFilter()" class="clean-all-filters btn show-for-tablet title filter-btn mobile-btn" title="<?php _ex('Återställ sökning', 'Produktkatalog', 'icebug') ?>"><?php _ex('Återställ sökning', 'Produktkatalog', 'icebug') ?><i class="icon-reply"></i></a>
	                <a ng-click="toggleMobileMenu()" class="title start-filtering-btn show-for-tablet"><?php _ex('Visa produkter', 'Produktkatalog', 'icebug') ?> <i class="icon icon-check-sign"></i></a>
	            </div>

	        </div><!--//products-cat-wrap-->
	    </div>
	</section>

	<section ng-cloak id="products-section" class="module secondary hide-for-no-js" role="main">
	    <div class="container product-listing-module">
	        <div id="products" class="row">

                <article class="span3 product mobile-module" ng-repeat="product in filteredProducts = ( products | filter:filterByTaxonomy | filter:textSearch )">
                    <div class="inner-product">
                        <a href="{{ product.permalink }}" title="{{ product.title }}">
                            <img ng-src="{{ product.image }}" alt=""/>
                            <h4 class="title">{{ decodeEntities(product.title) }}</h4>
                            <div class="product-terms">
                                <img ng-src="{{term.image}}" ng-repeat="term in product.terms" ng-if="termHasImage(term)" alt=""/>
                            </div>
                        </a>
                    </div>

                </article>

	            </div><!--//inner-products-->

                <article ng-if="filteredProducts.length < 1">
                    <h2><?php echo _x('Inga produkter matchar din sökning','Produktkatalog','icebug') ?></h2>
                </article>

	        </div>
	    </div>
	</section>

	<noscript>

	<?php

	$archive_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

	?>

	<section id="filter-section" class="module">
		<div class="container">

			<h5 class="title filter-btn show-for-tablet" data-signal="show"><?php echo _x('Filtrera','Produktkatalog','icebug') ?> <i class="icon icon-cog"></i></h5>

			<div class="products-cat-wrap">

				<div class="row products-cat-links">
					<div class="span3 products-cat-col">
						<div class="product-cat">
							<h5 class="title"><?php echo _x('Bäst för','Produktkatalog','icebug') ?></h5>
							<div class="product-cat-list-wrap">
								<ul><?php generate_terms_list($productTypeTerms); ?></ul>
							</div><!--//product-cat-list-wrap-->
						</div><!--//product-cat-->

						<div class="clearfix"></div>

						<div class="product-cat">
							<h5 class="title"><?php echo _x('Typ','Produktkatalog','icebug') ?></h5>
							<div class="product-cat-list-wrap">
								<ul><?php generate_terms_list($productGroupTerms); ?></ul>
							</div><!--//product-cat-list-wrap-->
						</div><!--//product-cat-->

					</div>
					<div class="span9 products-cat-col">
						<div class="row">
							<div class="product-cat">
								<h5 class="title"><?php echo _x('Kategori','Produktkatalog','icebug') ?></h5>
								<div class="product-cat-list-wrap">
									<ul><?php generate_terms_list($productCategoryTerms); ?></ul>
								</div><!--//product-cat-list-wrap-->
							</div><!--//product-cat-->

						</div>

						<div class="row product-cat-images-row">
							<div class="span3 product-cat product-cat-image">
								<h5 class="title"><?php echo _x('Grepp','Produktkatalog','icebug') ?></h5>
								<div class="product-cat-list-wrap">
									<ul><?php generate_terms_image_list($productTechGripTerms); ?></ul>
								</div><!--//product-cat-list-wrap-->
							</div>
							<div class="span3 product-cat product-cat-image product-cat-membrane">
								<h5 class="title"><?php echo _x('Membran','Produktkatalog','icebug') ?></h5>
								<div class="product-cat-list-wrap">
									<ul><?php generate_terms_image_list($productTechMembraneTerms); ?></ul>
								</div><!--//product-cat-list-wrap-->
							</div>
							<div class="span4 product-cat product-cat-image">
								<h5 class="title"><?php echo _x('Kyla','Produktkatalog','icebug') ?></h5>
								<div class="product-cat-list-wrap">
									<ul><?php generate_terms_image_list($productColdTerms); ?></ul>
								</div><!--//product-cat-list-wrap-->
							</div>
							<div class="span3 product-cat product-cat-image hide-for-tablet">
								<div class="product-cat-list-wrap">
									<a class="clean-all-filters btn filter-btn mobile-btn" href="<?php echo get_post_type_archive_link('product'); ?>" title="Rensa filtrering"><?php _ex('Återställ sökning', 'Produktkatalog', 'icebug') ?><i class="icon-reply"></i></a>
								</div><!--//product-cat-list-wrap-->
							</div>
						</div>
					</div>
					<a class="clean-all-filters btn show-for-tablet title filter-btn mobile-btn" href="<?php echo get_post_type_archive_link('product'); ?>" title="<?php _ex('Återställ sökning', 'Produktkatalog', 'icebug') ?>"><?php _ex('Återställ sökning', 'Produktkatalog', 'icebug') ?><i class="icon-reply"></i></a>
					<a href="<?php echo $archive_link ?>" class="title start-filtering-btn show-for-tablet"><?php _ex('Visa produkter', 'Produktkatalog', 'icebug') ?><i class="icon icon-check-sign"></i></a>
				</div>

			</div><!--//products-cat-wrap-->
		</div>
	</section>

	<section id="products-section" class="module secondary" role="main">
		<div class="container product-listing-module">
			<div id="products" class="row">
				<div class="inner-products">
					<?php
					$x = 0;
					$allProductsFiltered = filter_all_products($allProducts, $query_string);
					if(count($allProductsFiltered)) : foreach($allProductsFiltered as $p) : $x++;
						$post = get_post($p['ID']); setup_postdata($post);
						$product_terms = wp_get_object_terms($post->ID, $product_taxonomies, array('fields' => 'ids'));
						if( $x == 1 ) {
							echo '<div class="row">';
						} ?>
						<article class="span3 product mobile-module" id="post-<?php the_ID(); ?>">
							<div class="inner-product">
								<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
									<?php $product_image = get_field('images_product'); $product_image = $product_image[0]['image'];?>
									<img src="<?php echo ($src = $product_image) ? houston_resize($src,230,150,true) : get_stylesheet_directory_uri().'/images/default.png'; ?>" alt=""/>
									<h4 class="title"><?php the_title(); ?></h4>
									<div class="product-terms">
										<?php
										foreach($all_terms as $term){
											if(!in_array($term->term_id,$product_terms))
												continue;
											$default_term_id = icl_object_id($term->term_id, $term->taxonomy, true, icl_get_default_language());
											$image = houston_resize( get_field('icon_tax', $term->taxonomy . '_' . $default_term_id),false,32 );
											echo '<img src="' . $image .'" alt="" />';
										}
										?>
									</div>
								</a>
							</div>

						</article>

						<?php if ( $x == 4 || $y == count($allProductsFiltered) ) {
							echo '</div>'; $x = 0;
						} ?>

					<?php endforeach; wp_reset_postdata(); ?>
					<?php else : ?>
						<article>
							<h2><?php echo _x('Inga produkter matchar din sökning','Produktkatalog','icebug') ?></h2>
						</article>
					<?php endif; ?>
				</div><!--//inner-products-->
			</div>
		</div>
	</section>

	</noscript>

</div><!--//filtering-->
<?php get_footer(); ?>