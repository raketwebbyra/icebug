<?php
//---------------------------------------------------------------------------------
//	Require Files for Houston (Load with hook to be able to use with child theme)
//---------------------------------------------------------------------------------
add_action( 'after_setup_theme', 'parent_theme_setup', 9 );
function parent_theme_setup() {
	require_once(get_stylesheet_directory().'/lib/houston/houston_functions.php');
}

//---------------------------------------------------------------------------------
//	Development / Staging
//---------------------------------------------------------------------------------

if ( defined('WP_ENVIRONMENT') && WP_ENVIRONMENT == 'staging' ) {
	$current_user = wp_get_current_user();
	if ( 0 == $current_user->ID ) {
		if ( in_array( $GLOBALS['pagenow'], array( 'wp-login.php', 'wp-register.php' ) ) == false ) auth_redirect();
	}
}


//---------------------------------------------------------------------------------
//	Activate WP Core functionality
//---------------------------------------------------------------------------------
function houston_setup() {
	//add_theme_support('post-thumbnails');
	//add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));
	add_theme_support('menus');
	add_editor_style( 'editor-style.css' );
	register_nav_menus(array(
		'primary_navigation' => __('Huvudmeny'),
		'top_navigation' => __('Toppmeny'),
		'footer_navigation' => __('Sidfotmeny'),
		'mobile_primary_navigation' => __('Mobile Primary Navigation'),
	));


}
add_action('after_setup_theme', 'houston_setup');

register_sidebar(array('name'=> 'Sidebar',
	'before_widget' => '<ul id="%1$s" class="nav news-sidebar %2$s">',
	'after_widget' => '</ul>',
	'class' => '',
	'before_title' => '<li class="nav-header">',
	'after_title' => '</li>'
));


//---------------------------------------------------------------------------------
//	Filter WP Pages
//---------------------------------------------------------------------------------
function add_parent_class_nav( $css_class, $page, $depth, $args )
{
	if (!empty($args['has_children']))
		$css_class[] = 'parent';
	return $css_class;
}
add_filter( 'page_css_class', 'add_parent_class_nav', 10, 4 );


//---------------------------------------------------------------------------------
//	Wp Fonts
//---------------------------------------------------------------------------------
add_action('admin_head', 'my_custom_fonts');
function my_custom_fonts() {
	echo '<link rel="stylesheet" href="'.get_stylesheet_directory_uri().'/fonts/stylesheet.css" type="text/css" media="all" />';
}

//---------------------------------------------------------------------------------
//	Remove unneccesarry menu options in the wordpress backend.
//	Note: Be careful with this when cloning sites
//---------------------------------------------------------------------------------
function houston_remove_menus () {
	global $menu;
	$restricted = array(__('Dashboard'), __('Links'), __('Comments'), __('ether-ether'));
	end ($menu);
	while (prev($menu)){
		$value = explode(' ',$menu[key($menu)][0]);
		if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
	}
}
add_action('admin_menu', 'houston_remove_menus');



function get_element($path = false, $attr = false) {
	if(!$path) return false;
	if($attr) {
		extract($attr, EXTR_PREFIX_SAME, "block");
	}
	if (strpos($a,'.php') !== true) {
		$path .= '.php';
	}
	$file = get_stylesheet_directory().'/template-parts/elements/'.$path;
	if(!file_exists($file))
		return false;

	require($file);
}



//---------------------------------------------------------------------------------
//	Ajax load Press
//---------------------------------------------------------------------------------
function wp_infinitepaginate_press(){

	$paged           = $_POST['page_no'];
	$posts_per_page  = get_option('posts_per_page');
	# Load the posts
	query_posts(array('paged' => $paged, 'post_type' => 'press' ));
	get_template_part( 'template-parts/loops/loop-press');

	exit;
}

add_action('wp_ajax_infinite_scroll', 'wp_infinitepaginate_press');           // for logged in user
add_action('wp_ajax_nopriv_infinite_scroll', 'wp_infinitepaginate_press');


//---------------------------------------------------------------------------------
//	Custom Post Types
//---------------------------------------------------------------------------------

function houston_cpt_setup() {

	/*
	 *  Produkter
	 */
	$product_labels = array(
		'name' => __('Skor', 'post type general name', 'icebug'),
		'singular_name' => __('Produkt', 'post type singular name', 'icebug'),
		'add_new' => __('Lägg till'),
		'add_new_item' => __('Lägg till produkt', 'icebug'),
		'edit_item' => __('Redigera produkt', 'icebug'),
		'new_item' => __('Ny produkt', 'icebug'),
		'all_items' => __('Alla produkter', 'icebug'),
		'view_item' => __('Visa produkt', 'icebug'),
		'search_items' => __('Sök Butik', 'icebug'),
		'not_found' =>  __('Inget hittades', 'icebug'),
		'not_found_in_trash' => __('Inget hittades', 'icebug'),
		'parent_item_colon' => '',
		'menu_name' => __('Produkter', 'icebug')

	);
	$product_args = array(
		'labels' => $product_labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => _x( 'produkter', 'URL slug', 'icebug' ) ),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array( 'title', 'editor','custom-fields' )
	);
	register_post_type( 'product', $product_args );

	/* Category */
	$labels = array(
		'name'              => __( 'Kategorier', 'taxonomy general name' ),
		'singular_name'     => __( 'Kategori', 'taxonomy singular name' ),
		'search_items'      => __( 'Sök' ),
		'all_items'         => __( 'Alla' ),
		'parent_item'       => __( 'Förälder' ),
		'parent_item_colon' => __( 'Förälder:' ),
		'edit_item'         => __( 'Redigera' ),
		'update_item'       => __( 'Uppdatera' ),
		'add_new_item'      => __( 'Skapa ny', 'icebug' ),
		'new_item_name'     => __( 'Ny kategori' ),
		'menu_name'         => __( 'Kategorier' ),
	);
	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'rewrite' => array( 'slug' => _x( 'produktkategori', 'URL slug', 'icebug' ) ),

	);
	register_taxonomy( 'product_category', 'product', $args );




	/* Type */
	$labels = array(
		'name'              => __( 'Produkttyper', 'taxonomy general name' ),
		'singular_name'     => __( 'Produkttyp', 'taxonomy singular name' ),
		'search_items'      => __( 'Sök' ),
		'all_items'         => __( 'Alla' ),
		'parent_item'       => __( 'Förälder' ),
		'parent_item_colon' => __( 'Förälder:' ),
		'edit_item'         => __( 'Redigera' ),
		'update_item'       => __( 'Uppdatera' ),
		'add_new_item'      => __( 'Skapa ny' ),
		'new_item_name'     => __( 'Ny kategori' ),
		'menu_name'         => __( 'Typ' ),
	);
	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'rewrite' => array( 'slug' => _x( 'produkttyp', 'URL slug', 'icebug' ) ),

	);
	register_taxonomy( 'product_type', 'product', $args );

	/* Group */
	$labels = array(
		'name'              => __( 'Målgrupper', 'taxonomy general name' ),
		'singular_name'     => __( 'Målgrupp', 'taxonomy singular name' ),
		'search_items'      => __( 'Sök' ),
		'all_items'         => __( 'Alla' ),
		'parent_item'       => __( 'Förälder' ),
		'parent_item_colon' => __( 'Förälder:' ),
		'edit_item'         => __( 'Redigera' ),
		'update_item'       => __( 'Uppdatera' ),
		'add_new_item'      => __( 'Skapa ny' ),
		'new_item_name'     => __( 'Ny kategori' ),
		'menu_name'         => __( 'Målgrupp' ),
	);
	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'rewrite' => array( 'slug' => _x( 'produktgrupp', 'URL slug', 'icebug' ) ),

	);
	register_taxonomy( 'product_group', 'product', $args );



	/* Grip */
	$labels = array(
		'name'              => __( 'Grepp', 'taxonomy general name' ),
		'singular_name'     => __( 'Grepp', 'taxonomy singular name' ),
		'search_items'      => __( 'Sök' ),
		'all_items'         => __( 'Alla' ),
		'parent_item'       => __( 'Förälder' ),
		'parent_item_colon' => __( 'Förälder:' ),
		'edit_item'         => __( 'Redigera' ),
		'update_item'       => __( 'Uppdatera' ),
		'add_new_item'      => __( 'Skapa ny' ),
		'new_item_name'     => __( 'Ny teknologi' ),
		'menu_name'         => __( 'Grepp' ),
	);
	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'rewrite' => array( 'slug' => _x( 'teknik-grepp', 'URL slug', 'icebug' ) ),

	);
	register_taxonomy( 'product_tech_grip', 'product', $args );


	/* Membrane */
	$labels = array(
		'name'              => __( 'Membran', 'taxonomy general name' ),
		'singular_name'     => __( 'Membran', 'taxonomy singular name' ),
		'search_items'      => __( 'Sök' ),
		'all_items'         => __( 'Alla' ),
		'parent_item'       => __( 'Förälder' ),
		'parent_item_colon' => __( 'Förälder:' ),
		'edit_item'         => __( 'Redigera' ),
		'update_item'       => __( 'Uppdatera' ),
		'add_new_item'      => __( 'Skapa ny' ),
		'new_item_name'     => __( 'Ny teknologi' ),
		'menu_name'         => __( 'Membran' ),
	);
	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'rewrite' => array( 'slug' => _x( 'teknik-membran', 'URL slug', 'icebug' ) ),

	);
	register_taxonomy( 'product_tech_membrane', 'product', $args );



	/* Cold */
	$labels = array(
		'name'              => __( 'Kyla', 'taxonomy general name' ),
		'singular_name'     => __( 'Kyla', 'taxonomy singular name' ),
		'search_items'      => __( 'Sök' ),
		'all_items'         => __( 'Alla' ),
		'parent_item'       => __( 'Förälder' ),
		'parent_item_colon' => __( 'Förälder:' ),
		'edit_item'         => __( 'Redigera' ),
		'update_item'       => __( 'Uppdatera' ),
		'add_new_item'      => __( 'Skapa ny' ),
		'new_item_name'     => __( 'Ny teknologi' ),
		'menu_name'         => __( 'Kyla' ),
	);
	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'rewrite' => array( 'slug' => _x( 'teknik-kyla', 'URL slug', 'icebug' ) ),

	);
	register_taxonomy( 'product_tech_cold', 'product', $args );

	/*
	 *  Recensioner
	 */
	$testemonial_labels = array(
		'name' => _x('Recensioner', 'post type general name', 'icebug'),
		'singular_name' => _x('Recension', 'post type singular name', 'icebug'),
		'add_new' => __('Skapa ny'),
		'add_new_item' => __('Lägg till', 'icebug'),
		'edit_item' => __('Redigera', 'icebug'),
		'new_item' => __('Ny recension', 'icebug'),
		'all_items' => __('Alla recensioner', 'icebug'),
		'view_item' => __('Visa recension', 'icebug'),
		'search_items' => __('Sök', 'icebug'),
		'not_found' =>  __('Inget hittades', 'icebug'),
		'not_found_in_trash' => __('Inget hittades', 'icebug'),
		'parent_item_colon' => '',
		'menu_name' => __('Recensioner', 'icebug')

	);
	$testemonial_args = array(
		'labels' => $testemonial_labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => _x( 'press', 'URL slug', 'icebug' ) ),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' )
	);
	register_post_type( 'testemonial', $testemonial_args );



	/*
	 *  Annonser
	 */
	$product_labels = array(
		'name' => _x('Annonser', 'post type general name', 'icebug'),
		'singular_name' => _x('Annons', 'post type singular name', 'icebug'),
		'add_new' => __('Lägg till', 'icebug'),
		'add_new_item' => __('Lägg till', 'icebug'),
		'edit_item' => __('Redigera', 'icebug'),
		'new_item' => __('Ny ', 'icebug'),
		'all_items' => __('Alla ', 'icebug'),
		'view_item' => __('Visa ', 'icebug'),
		'search_items' => __('Sök ', 'icebug'),
		'not_found' =>  __('Inget hittades', 'icebug'),
		'not_found_in_trash' => __('Inget hittades', 'icebug'),
		'parent_item_colon' => '',
		'menu_name' => __('Annonser', 'icebug')

	);
	$product_args = array(
		'labels' => $product_labels,
		'public' => false,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => _x( 'ads', 'URL slug', 'icebug' ) ),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array( 'title' )
	);
	register_post_type( 'ad', $product_args );

	/*
	 *  Återförsäljare
	 */

	$retailer_labels = array(
		'name' => _x('Återförsäljare', 'post type general name', 'icebug'),
		'singular_name' => _x('Återförsäljare', 'post type singular name', 'icebug'),
		'add_new' => __('Lägg till', 'icebug'),
		'add_new_item' => __('Lägg till', 'icebug'),
		'edit_item' => __('Redigera', 'icebug'),
		'new_item' => __('Ny ', 'icebug'),
		'all_items' => __('Alla ', 'icebug'),
		'view_item' => __('Visa ', 'icebug'),
		'search_items' => __('Sök ', 'icebug'),
		'not_found' =>  __('Inget hittades', 'icebug'),
		'not_found_in_trash' => __('Inget hittades', 'icebug'),
		'parent_item_colon' => '',
		'menu_name' => __('Återförsäljare', 'icebug')
	);

	$retailer_args = array(
		'labels' => $retailer_labels,
		'public' => false,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => _x( 'retailers', 'URL slug', 'icebug' ) ),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array( 'title' )
	);
	register_post_type( 'retailer', $retailer_args );

}
add_action('after_setup_theme', 'houston_cpt_setup');




function add_nav_class( $classes, $item ){
	if(is_post_type_archive('product') || is_singular('product')) {
		if($item->url == rtrim(get_post_type_archive_link('product'), '/')) {
			$classes[] = "active";
		}
	}
	return $classes;
}
add_filter('nav_menu_css_class' , 'add_nav_class' , 10 , 2);


//---------------------------------------------------------------------------------
//	ACF Settings
//---------------------------------------------------------------------------------
if(function_exists("acf_add_options_sub_page"))
{
	acf_add_options_sub_page(array(
		'title' => 'Pressinställningar',
		'parent' => 'edit.php?post_type=press',
		'capability' => 'manage_options'
	));

	acf_add_options_sub_page(array(
		'title' => 'Inställningar',
		'parent' => 'edit.php?post_type=offer',
		'capability' => 'manage_options'
	));
}

/* Include ACF Location Field  */

include_once('lib/acf-location-field/acf-location.php');

//---------------------------------------------------------------------------------
//	Font awesome menu
//---------------------------------------------------------------------------------
function fontawesome_menu( $nav ) {
	$menu = preg_replace_callback(
		'/<li((?:[^>]+)(icon-[^ ]+ )(?:[^>]+))><a[^>]+>(.*)<\/a>/',
		'fontawesome_replace',
		$nav
	);
	print $menu;
}
function fontawesome_replace( $a ) {
	$listitem = $a[0];
	$icon = $a[2];
	$link_text = $a[3];
	$str_noicon = str_replace( $icon, '', $listitem );
	$str = str_replace( $link_text, '<i class="' . trim( $icon ) . '"></i><span class="fontawesome-text"> ' . $link_text . '</span>', $str_noicon );
	return $str;
}
add_filter( 'wp_nav_menu' , 'fontawesome_menu', 10, 2);


//---------------------------------------------------------------------------------
//	Allow sessions
//---------------------------------------------------------------------------------

add_action('init', 'myStartSession', 1);
add_action('wp_logout', 'myEndSession');
add_action('wp_login', 'myEndSession');

function myStartSession() {
	if(!session_id()) {
		session_start();
	}
}

function myEndSession() {
	session_destroy ();
}

//---------------------------------------------------------------------------------
//	Number of products on Products page
//---------------------------------------------------------------------------------

add_filter('pre_get_posts', 'Per_category_basis');

function Per_category_basis($query){

	if ($query->is_archive) {

		if ( $query->query['post_type'] == 'product' ) {
			$query->set('posts_per_page', -1);
		}
	}

	return $query;

}

//---------------------------------------------------------------------------------
//	Register settings pages.
//---------------------------------------------------------------------------------

acf_add_options_sub_page(array(
	'title' => 'Produktinställningar',
	'parent' => 'edit.php?post_type=product',
	'capability' => 'manage_options'
));

acf_add_options_sub_page(array(
	'title' => 'Globala Inställningar',
	'menu' => 'Globala Inställningar',
	'capability' => 'manage_options'
));

//---------------------------------------------------------------------------------
//	Custom Excerpt
//---------------------------------------------------------------------------------

function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$readmore_text = _x('Läs mer', 'Nyheter', 'icebug');
		$readmore_link = '<a href="' . get_permalink() . '" title="' . get_the_title() . '">'. $readmore_text .' <i class="icon-caret-right"></i></a>';
		$excerpt = implode(" ",$excerpt).'... ' . $readmore_link;
	} else {
		$excerpt = implode(" ",$excerpt);
	}
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}

//---------------------------------------------------------------------------------
//	Custom Ancestor
//---------------------------------------------------------------------------------

function remove_parent($var)
{
	// check for current page values, return false if they exist.
	if ($var == 'current_page_parent' || $var == 'current-menu-item' || $var == 'current-page-ancestor') { return false; }
	return true;
}

function tg_add_class_to_menu($classes)
{
	// team is my custom post type
	if ( is_post_type_archive() )
	{

		if ( !is_singular('product') ) {
			// we're viewing a custom post type, so remove the 'current-page' from all menu items.
			$classes = array_filter($classes, "remove_parent");

			// add the current page class to a specific menu item.
			if (in_array('menu-item-16', $classes) || in_array('menu-item-128', $classes)) $classes[] = 'current-page-ancestor';
		}
	}

	return $classes;
}

add_filter('nav_menu_css_class', 'tg_add_class_to_menu');


//---------------------------------------------------------------------------------
//	List all categories
//---------------------------------------------------------------------------------

$news_icl = icl_object_id( get_page_by_path('nyheter')->ID , 'page', true);

function icebug_list_all_categories() {
	global $news_icl;
	$news_icl = icl_object_id( get_page_by_path('nyheter')->ID , 'page', true);
	$categories = get_categories();
	$current_cat = get_the_category();
	// Output sub-menu

	$link_class = '';
	if ( $current_cat == false ) {
		$link_class = 'current_page_item';
	}

	echo '<ul class="sub-menu">';
	echo '<li class="'.$link_class.'"><a href="'. get_permalink($news_icl) . '">' . _x('Alla nyheter', 'Nyheter', 'icebug') . '</a></li>';
	foreach ( $categories as $cat ) {
		$link_class = '';
		if ( $cat->term_id == $current_cat[0]->term_id ) {
			$link_class = 'current_page_item';
		}
		echo '<li class="'.$link_class.'"><a href="' . get_term_link($cat) . '">' . $cat->name . '</a></li>';
	}
	echo '</ul>';
}

/* Mobile List all Categories */


function icebug_list_all_categories_mobile() {
	global $news_icl;
	$news_icl = icl_object_id( get_page_by_path('nyheter')->ID , 'page', true);
	echo '<ul class="dl-menu">';
	echo '<li><a href="'. get_permalink($news_icl) . '">' . _x('Alla nyheter', 'Nyheter', 'icebug') . '</a></li>';
	wp_list_categories(array('title_li' => ''));
	echo '</ul>';
}


//---------------------------------------------------------------------------------
//	Ajax
//---------------------------------------------------------------------------------
function get_360_images(){
	while(the_repeater_field('360_images_product', $_POST['id'])):
		echo '<img src="'.houston_resize(get_sub_field('image'), false, 450 , false).'" />';
	endwhile;
	die();
}

add_action('wp_ajax_get_360_images', 'get_360_images');
add_action('wp_ajax_nopriv_get_360_images', 'get_360_images');

//---------------------------------------------------------------------------------
//  Get Languange Version of Field
//---------------------------------------------------------------------------------

function get_field_by_language($field_name, $post_id) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}

	global $sitepress;
	$default_language = $sitepress->get_default_language();

	if ( ICL_LANGUAGE_CODE == $default_language ) {
		return get_field($field_name, $post_id);
	} else {
		return get_field(ICL_LANGUAGE_CODE . '_' . $field_name, $post_id);
	}
}

//---------------------------------------------------------------------------------
//  Get Languange Version of Field
//---------------------------------------------------------------------------------

function wpml_filter_fix($string){
	$string = str_replace('@'.ICL_LANGUAGE_CODE, '', $string);
	return $string;
}

//---------------------------------------------------------------------------------
//  Update Retailer
//---------------------------------------------------------------------------------

function update_retailer_geolocation( ) {

	$slug = 'retailer';


	if ( $slug != $_POST['post_type'] ) {
		return;
	}


	$args = array(
		'post_type' => 'retailer',
		'showposts' => -1,
	);

	$retailers = get_posts($args);
	$field_key = 'field_52455a89aa7e7';

	foreach ( $retailers as $ret ) {
		global $post;
		$post = $ret;
		setup_postdata($post);
		$address = get_field('retailer_post_address') . ' ' . get_field('retail_address');
		//$geolocation = geocoder::getLocation($address);
		if ( $geolocation ) {
			$lat_lng = $geolocation['lat'] . ',' . $geolocation['lng'];
			update_field($field_key, $lat_lng);
		}
		wp_reset_postdata();
	}


}
add_action('save_post', 'update_retailer_geolocation');

//	Generate JSON file for JetShop Menu integration
//---------------------------------------------------------------------------------
function icebug_json_menu( $post_id ) {

	$topMenu = array();
	$mainMenu = array();



	// populate TopMenu
	$topMenuItems = wp_get_nav_menu_items( 26 );
	foreach ($topMenuItems as $item) {
		$val = array(
			'id' => $item->object_id,
			'title' => $item->title,
			'url' => $item->url,
			'target' => $item->target,
			'attr_title' => $item->attr_title,
			'description' => $item->description,
			'classes' => $item->classes
		);
		array_push($topMenu,$val);
	}


	// populate mainMenu
	$mainMenuItems = wp_get_nav_menu_items( 2 );
	foreach ($mainMenuItems as $item) {
		$val = array(
			'id' => $item->object_id,
			'title' => $item->title,
			'url' => $item->url,
			'target' => $item->target,
			'attr_title' => $item->attr_title,
			'description' => $item->description,
			'classes' => $item->classes
		);
		array_push($mainMenu,$val);
	}

	$menu = array(
		"slogan" => _x('Swedish footwear for the new outdoor','Slogan', 'icebug'),
		"topMenu" => $topMenu,
		"mainMenu" => $mainMenu
	);

	$fp = fopen(ABSPATH . '/menu.json', 'w');
	fwrite($fp, "func(".json_encode($menu).")");
	fclose($fp);
}
add_action( 'save_post', 'icebug_json_menu' );



function newsletterAjaxFunction(){
	//get the data from ajax() call
	global $wpdb;
	$table_name = $wpdb->prefix . "newsletter";

	/*
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
		//table is not created. you may create the table here.
		$sql = "CREATE TABLE $table_name (
		  id int NOT NULL AUTO_INCREMENT,
		  email varchar NOT NULL,
		  code varchar NOT NULL
		);";
		//reference to upgrade.php file
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}
	*/

	//@TODO Escapar emailen så att den inte kan injectas
	//@TODO Du la till wp_newsletter på din lokala fast siten körs på rakets stagin databas
	//@TODO Jag la till tabellen på stagingdatabasen på raket
	//@TODO Nu fungerar din sql
	$exists = $wpdb->get_var( "SELECT COUNT(*) FROM wp_newsletter WHERE email = '" . mysql_real_escape_string($_POST['email']) . "'" );
	if($exists == 0) {

		// No record
		$wpdb->insert($table_name, array(
			'email' => $_POST['email'],
			'code' => $_POST['code'],
			'subscribed' => current_time('mysql', 1)
		));

		$return = array(
			'status'    => 1,
			'msg'       => _x('Tack för din anmälan!','Newsletter','icebug')
		);
		echo json_encode($return);

	} else {

		$return = array(
			'status'    => 0,
			'msg'       => _x('E-posten är redan registrerad','Newsletter','icebug')
		);
		echo json_encode($return);
	}
	die();
}
// creating Ajax call for WordPress
add_action( 'wp_ajax_nopriv_newsletterAjaxFunction', 'newsletterAjaxFunction' );
add_action( 'wp_ajax_newsletterAjaxFunction', 'newsletterAjaxFunction' );

/* --------------------------------
|
|   - Product Ajax
|
----------------------------------*/

function fetch_all_products($queryString = null) {
	// The $_REQUEST contains all the data sent via ajax
	if ( isset($_REQUEST) ) {

		// do terms query once, instead of every product
		$product_taxonomies = array(
			'product_tech_grip',
			'product_tech_membrane',
			'product_tech_cold',
			'product_type',
			'product_group',
			'product_category',
		);

		$visible_taxonomies = array(
			'product_tech_grip',
			'product_tech_membrane',
			'product_tech_cold',
		);

		$all_terms = array();
		foreach($product_taxonomies as $taxonomy){
			$terms = get_terms($taxonomy);
			foreach ( $terms as $term ) {
				$all_terms[$term->term_id] = $term;
			}
		}

		$products = array();
		if($queryString) {
			$args = $queryString;
		} else {
			$args = array('post_type' => 'product', 'order_by' => 'menu_order');
		}

		$query = new WP_Query($args);
		while ( $query->have_posts() ) : $query->the_post();
			global $post;

			$product_image = get_field('images_product'); $product_image = $product_image[0]['image'];
			$product_terms = wp_get_object_terms($post->ID, $product_taxonomies, array('fields' => 'ids'));

			$terms = array();
			foreach ( $all_terms as $term ) {
				if(!in_array($term->term_id,$product_terms))
					continue;
				$defaultTermID = icl_object_id($term->term_id, $term->taxonomy, true, icl_get_default_language());
				$terms[$term->term_id] = array(
					'slug' => $term->slug,
					'taxonomy' => $term->taxonomy
				);

				if(in_array($term->taxonomy, $visible_taxonomies)) {
					$terms[$term->term_id]['image'] = houston_resize( get_field('icon_tax', $term->taxonomy . '_' . $defaultTermID),false,32);
				}
			}

			$products[] = array(
				'ID' => $post->ID,
				'title' => get_the_title($post->ID),
				'permalink' => get_permalink($post->ID),
				'image' => $product_image ? houston_resize($product_image,230,150,true) : get_stylesheet_directory_uri().'/images/default.png',
				'terms' => $terms
			);

		endwhile;

		return $products;
	}
}

function filter_all_products($allProducts, $query_string) {
	$filteredProducts = array();
	$active_taxonomies = array();

	// Create array of query string
	$queryStrings = explode('&', $query_string);
	foreach ( $queryStrings as $qs ) {
		$qsExploded = explode('=', $qs);
		$taxonomy = $qsExploded[0];
		$terms = explode('%2C', $qsExploded[1]);

		if(!in_array($taxonomy, $active_taxonomies)) {
			$active_taxonomies[$taxonomy] = $terms;
		}
	}

	// Remove post type from array
	unset($active_taxonomies['post_type']);

	// Go through every product and check if it has terms in all taxonomies
	foreach($allProducts as $product) {
		$productActiveTermCount = 0;
		foreach($product['terms'] as $term) {
			foreach($active_taxonomies as $taxonomy => $activeTerms) {
				foreach($activeTerms as $activeTerm) {
					if($term['slug'] == $activeTerm) $productActiveTermCount++;
				}
			}
		}

		if($productActiveTermCount >= count(array_keys($active_taxonomies))) {
			$filteredProducts[] = $product;
		}
	}

	return $filteredProducts;
}

$archive_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

function get_term_list($tax)
{
	global $archive_link;
	$current_terms = array_filter(explode(',',$_GET[$tax]));
	$terms = get_terms($tax, array('hide_empty' => false));
	$term_list = array();

	foreach($terms as $term) {
		$term_list_item = array();
		$term_list_item['popover_info'] = ($term->description) ? 'popover-info':'';

		if(in_array($term->slug, $current_terms)) {
			$term_list_item['class'] = 'mobile-btn btn active';
			$term_list_item['icon'] = 'icon-ok-sign';
			$term_list_item['icon_hover'] = 'icon-minus-sign';
			$term_list_item['link'] = add_query_arg($tax, implode(',',array_diff($current_terms,array($term->slug))), $archive_link);
		} else {
			$term_list_item['class'] = 'btn mobile-btn';
			$term_list_item['icon'] = 'icon-minus-sign';
			$term_list_item['icon_hover'] = 'icon-plus-sign';

			$this_terms = $current_terms;
			array_push($this_terms, $term->slug);
			$term_list_item['link'] = add_query_arg($tax, implode(',', $this_terms), $archive_link);
		}
		$term_list_item['taxonomy'] = $term->taxonomy;
		$term_list_item['slug'] = $term->slug;
		$term_list_item['description'] = $term->description;
		$term_list_item['name'] = $term->name;
		$term_list_item['term_id'] = $term->term_id;
		$term_list[] = $term_list_item;
	}

	return $term_list;
}

function get_term_image_list($tax)
{
	global $archive_link;
	$current_terms = array_filter(explode(',',$_GET[$tax]));
	$terms = get_terms($tax, array('hide_empty' => false));
	$term_list = array();

	foreach($terms as $term) {
		$term_list_item['popover_info'] = ($term->description) ? 'popover-info':'';
		if(in_array($term->slug, $current_terms)) {
			$term_list_item['class'] = 'icon active';
			$term_list_item['mobile_class'] = 'active mobile-btn btn mobile-term-image-btn show-for-tablet';
			$term_list_item['icon'] = 'icon-ok-sign';
			$term_list_item['icon_hover'] = 'icon-minus-sign';
			$term_list_item['link'] = add_query_arg($tax, implode(',',array_diff($current_terms,array($term->slug))), $archive_link);
		} else {
			$term_list_item['class'] = 'icon inactive';
			$term_list_item['mobile_class'] = 'inactive mobile-btn btn mobile-term-image-btn show-for-tablet';
			$term_list_item['icon'] = 'icon-minus-sign';
			$term_list_item['icon_hover'] = 'icon-plus-sign';

			$this_terms = $current_terms;
			array_push($this_terms, $term->slug);
			$term_list_item['link'] = add_query_arg($tax, implode(',', $this_terms), $archive_link);
		}

		// Use image from "primary language"
		$default_term_id = icl_object_id($term->term_id, $tax, true, icl_get_default_language());
		$image = houston_resize( get_field('icon_tax', $tax . '_' . $default_term_id),false,32 );
		$image_bw = houston_resize( get_field('icon_tax', $tax . '_' . $default_term_id),false,32, false, false, true );

		$term_list_item['taxonomy'] = $term->taxonomy;
		$term_list_item['slug'] = $term->slug;
		$term_list_item['description'] = $term->description;
		$term_list_item['name'] = $term->name;
		$term_list_item['term_id'] = $term->term_id;
		$term_list_item['image'] = $image;
		$term_list_item['image_bw'] = $image_bw;

		$term_list[] = $term_list_item;

	}

	return $term_list;
}

function generate_terms_list($terms) {
	foreach($terms as $term) {
		echo '<li>';
		echo '<a data-taxonomy="'. $term['taxonomy'] .'" data-termslug="' . $term['slug'] . '" data-content="'.$term['description'].'" class="'.$term['popover_info'].' '.$term['class'].'" href="'.$term['link'].'" title="'.$term['name'].'">'.$term['name'].' <i class="'.$term['icon'].'"></i> <i class="hover '.$term['icon_hover'].'"></i></a>';
		echo '</li>';
	}
}

function generate_terms_image_list($terms) {
	foreach($terms as $term) {
		echo '<li>';
		echo '<a data-taxonomy="'.$term['taxonomy'].'" data-termslug="'.$term['slug'].'" data-content="'.$term['description'].'" class="hide-for-tablet '.$term['popover_info'].' '.$term['class'].'" href="'.$term['link'].'" title="'.$term['name'].'"><img class="color-image" src="'.$term['image'].'" alt="" title="" /><img class="bw-image" src="'.$term['image_bw'].'" alt="" title="" /></a>';
		echo '<a data-taxonomy="'. $term['taxonomy'] .'" data-termslug="' . $term['slug'] . '" data-content="'.$term['description'].'" class="show-for-tablet '.$term['popover_info'].' '.$term['class'].'" href="'.$term['link'].'" title="'.$term['name'].'">'.$term['name'].' <i class="'.$term['icon'].'"></i> <i class="hover '.$term['icon_hover'].'"></i></a>';
		echo '</li>';
	}
}


add_action( 'wp_ajax_fetch_all_products', 'fetch_all_products' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
// add_action( 'wp_ajax_nopriv_example_ajax_request', 'example_ajax_request' );



/* --------------------------------
|
|   - Hex
|
----------------------------------*/

function hex2rgb($hex) {
	$hex = str_replace("#", "", $hex);

	if(strlen($hex) == 3) {
		$r = hexdec(substr($hex,0,1).substr($hex,0,1));
		$g = hexdec(substr($hex,1,1).substr($hex,1,1));
		$b = hexdec(substr($hex,2,1).substr($hex,2,1));
	} else {
		$r = hexdec(substr($hex,0,2));
		$g = hexdec(substr($hex,2,2));
		$b = hexdec(substr($hex,4,2));
	}
	$rgb = array($r, $g, $b);
	//return implode(",", $rgb); // returns the rgb values separated by commas
	return $rgb; // returns an array with the rgb values
}