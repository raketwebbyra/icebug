<?php get_header(); ?>
<?php get_template_part('/template-parts/mobile/mobile-sidebar'); ?>
<?php get_template_part('/template-parts/breadcrumbs'); ?>
    <section id="main-section" role="main">
        <div class="container">
            <div class="row">

                <aside id="sidebar" class="span3 hide-for-mobile">

                    <?php

                    icebug_list_all_categories();

                    // Ad loop
                    include(get_stylesheet_directory().'/template-parts/ad-loop.php'); ?>

                </aside>

                <div id="page-content" class="span9" <?php live_edit('post_title, post_content, news_slider') ?>>

                    <?php get_template_part('template-parts/post-slider'); ?>

                    <?php get_template_part('template-parts/loops/loop', 'single'); ?>

                </div>

            </div>
        </div><!--//container-->
    </section>
<?php get_footer(); ?>