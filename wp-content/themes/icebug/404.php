<?php

get_header(); ?>

    <section id="main-section" role="main">
        <div class="container">
            <div class="row">

                <h1>Sidan du letar efter kan inte hittas!</h1>
                <p>Vi ber om ursäkt för detta!</p>
                <a href="<?php echo get_bloginfo('url'); ?>" title="Tillbaka till startsidan">&laquo; Tillbaka till startsidan</a>

            </div>
        </div>
    </section>
<?php get_footer(); ?>