
(function ($, window, undefined) {
    'use strict';

    var isMobile = false,
        isTablet = false,
        oldie = false;

    // If mobile screen size
    if ( Modernizr.mq("screen and (max-width:767px)") ) {
        isMobile = true;
    }

    // If tablet screen size
    if ( Modernizr.mq("screen and (max-width:1023px)") ) {
        isTablet = true;
    }

    // If browser is less than IE9
    if ( $('html').hasClass('oldie') ) {
        oldie = true;
    }

    /* ---------------------------
     |
     | - Fetching Products
     |
     ---------------------------- */

    var products = $('#products'),
        newProducts;

     function FetchProducts(fetchURL) {

        $.get(fetchURL, function (results) {
            newProducts = $(results).find('#products').children();
            $(document).trigger('new-products-fetched');

            if ( !oldie ) {
                window.history.pushState({}, 'Produkter', fetchURL);
            }

        });
        return false;
     }

     function FadeOutCurrentProducts(products) {
        products.css({ 'height': 'auto' })
        .children()
            .css('position', 'relative')
            .stop().animate({
            opacity: 0,
            left: products.width() / 2
        }, function () {
            $(this).remove();
            $(document).trigger('current-products-faded-out')
        });
     }

     function FadeInNewProducts(products, newProducts) {

         removeLoader('#products');

        //Append the new products with no opacity
        products.append( newProducts.css('opacity', 0) );
        products.children()
            .css({
                position: 'relative',
                left : '-' + (products.width() / 2) + 'px'
            })
            .animate({
            opacity : 1,
            left: 0
        }, function() {
            $(document).trigger('new-products-faded-in')
        })
     }

     // # Event Chain

     // Listen for when new products has been fetched
    $(document).bind('new-products-fetched', function() {
        FadeOutCurrentProducts(products);
    });

    // Listen for when current products has been faded out
    $(document).bind('current-products-faded-out', function() {
        FadeInNewProducts(products, newProducts);
    });

    

    /* ---------------------------
     |
     | - Filtering Products
     |
     ---------------------------- */


    var Filtering = {

        init: function () {
            this.callURL = $(location).attr('href');
            this.initLinks();
            this.fetchTimer;
        },

        initLinks: function() {

            var self = this;

            $('#filter-section').find('a').on('click', function (e) {
                e.preventDefault();

                addLoader('#products');

                if ( $(this).hasClass('clean-all-filters') ) {
                    $.each( $('#filter-section').find('a.active'), function(index, value) {
                        changeLinkState($(value), 'removeActive');
                        self.callURL = parseURL( self.callURL, $(value), 'remove' );
                    });
                    FetchProducts($(this).attr('href'));
                    return;
                }

                if ( $(this).hasClass('active') === false ) {
                    changeLinkState($(this), 'addActive');
                    self.callURL = parseURL( self.callURL, $(this), 'add' );
                } else {
                    changeLinkState($(this), 'removeActive');
                    self.callURL = parseURL( self.callURL, $(this), 'remove' );
                }

                
                // If another link is clicked within this timewindow, then just stack the url and reset the timer
                if ( self.fetchTimer ) {
                    window.clearTimeout(self.fetchTimer)
                }

                // A timer that starts when a term is clicked
                self.fetchTimer = window.setTimeout(function(){
                    // When the timer runs out, fetch the new products
                    FetchProducts(self.callURL)
                }, 1200);

            });            

            // Popover for terms is initialized with the standard Filtering function
            if ( isMobile === false ) {
                termPopOver();
            }

        }

    }

    // If we're viewing in desktop mode, use standard filtering
    if ( isTablet === false ) {
        Filtering.init();
    }


    /* ---------------------------
     |
     | - Mobile Filtering
     |  
     ---------------------------- */

    var MobileFiltering = {

        init: function() {
            this.initMobileItems();
            this.isFiltersBtnVisible = false;
            this.filteringBtn = $('.start-filtering-btn');
            this.callURL = this.filteringBtn.attr('href');
            this.checkFilteringBtn();
        },

        // Show filters function
        showFilters: function(signal) {

            var filtersHeight = 0;

            if ( signal == 'show' ) {
                filtersHeight = $('.products-cat-wrap').find('.products-cat-links').height();
            }

            $('.products-cat-wrap').stop().animate({
                height: filtersHeight
            }, function() {
                if ( signal == 'show' ) {
                    $(this).css('height', 'auto');
                }
            })
        },

        // Animate the menu main categories
        animateMenuItem: function( menuItem, animHeight ) {

            if ( menuItem.hasClass('cat-open') ) {
                menuItem.stop().animate({
                    height : 0
                }, function() {
                    $(this).removeClass('cat-open');
                })
            } else {
                menuItem.stop().animate({
                    height : animHeight
                }, function() {
                    $(this).addClass('cat-open');
                })
            }


        },

        // Animate the filtering button
        animateFilteringBtn: function(signal) {

            var animation = { height: 0, opacity: 0, padding: 0 };

            if ( signal === 'show' ) {
                animation['height'] = 50;
                animation['opacity'] = 1;
                animation['padding'] = '15px 0';
                this.isFiltersBtnVisible = true;
            } else {
                this.isFiltersBtnVisible = false;
            }

            this.filteringBtn.animate({
                height: animation['height'],
                opacity: animation['opacity'],
                padding: animation['padding']
            })
        },

        // Check how many filters has been klicked
        checkFilteringBtn: function() {

            var self = this;

            this.filtersClicked = 0;

            // For every term in the menu that has the current state of active, increment the number of filters clicked
            $('.product-cat-list-wrap .mobile-btn').each(function(index, link) {
                if ( $(link).hasClass('active') ) {
                    self.filtersClicked++;
                }
            });

            // If there are filters active, then show the filter button
            if ( this.filtersClicked > 0 ) {
                this.animateFilteringBtn('show');
            }

        },

        // Make the filtering links work as mobile
        initMobileItems: function() {

            var self = this;

            $('.products-cat-col').find('.title').on('click', function(e) {
                e.preventDefault();

                var catListWrap = $(this).parent().find('.product-cat-list-wrap'),
                    listHeight = $(this).parent().find('ul').height();

                self.animateMenuItem(catListWrap, listHeight);
                self.animateMenuItem( $('.products-cat-col').find('.cat-open'), listHeight);

            });

        }

    }

    // If the page is viewed in tablet mode, then fire the Mobile filtering functions
    if ( isTablet === true ) {
        
        //Shorten the object litteral name    
        var mf = MobileFiltering;

        mf.init();

        // Hide / Show the Filtering menu
        $('.filter-btn').on('click', function(e){
            e.stopPropagation();
            mf.showFilters( $(this).data('signal') );
            if ( $(this).data('signal') == 'show' ) {
                $(this).data('signal', 'hide');
            } else {
                $(this).data('signal', 'show');
            }
        });

        // When a term has been clicked
        $('.mobile-btn').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            if ( $(this).hasClass('clean-all-filters') ) {
                addLoader('#products');

                $.each( $('#filter-section').find('a.active'), function(index, value) {
                    changeLinkState($(value), 'removeActive');
                });

                $.each( $('#filter-section').find('.product-cat'), function(index, cat) {
                    var catWrap = $(cat).find('.product-cat-list-wrap');
                    if ( catWrap.hasClass('cat-open') ) {
                        catWrap.parent().find('.title').click();
                    }
                });

                FetchProducts($(this).attr('href'));
                mf.showFilters( 'hide' );
                $('.filter-btn').data('signal', 'show');
                return;
            }

            // If the term is not currently active, add the terms parameter values to the URL stack
            if ( !$(this).hasClass('active') ) {
                
                mf.callURL = parseURL( mf.callURL, $(this), 'add');
                mf.filteringBtn.attr('href', mf.callURL);

                // Change the links' state to active
                changeLinkState($(this), 'addActive');

                mf.checkFilteringBtn();

            } else {
                
                // If the term is currently active, then remove the parameter values from the url stack
                mf.callURL = parseURL( mf.callURL, $(this), 'remove');
                mf.filteringBtn.attr('href', mf.callURL);

                // And change the link state accordingly
                changeLinkState($(this), 'removeActive');

                mf.checkFilteringBtn();
            }
        });

        // The final step of filtering is triggering the AJAX call against the server
        $('.start-filtering-btn').on('click', function(e) {
            e.preventDefault();

            // Fetch new the new products
            FetchProducts($(this).attr('href'));

            // Animate Window to top of products section
            $('html, body').animate({
                scrollTop : $('#filter-section').offset().top
            })

            // Hide filtering menu
            mf.showFilters( 'hide' );
            $('.filter-btn').data('signal', 'show');

        });

    }

    /* ---------------------------
     |
     | - Change Link State
     |  
     ---------------------------- */

     function changeLinkState(link, signal) {

        if ( signal == 'addActive' ) {
            link.addClass('active');
            link.removeClass('inactive-mobile')
            link.find('.icon-minus-sign').removeClass('icon-minus-sign').addClass('icon-ok-sign');
            link.find('.hover').removeClass('icon-plus-sign').addClass('icon-minus-sign');
        } else {
            link.removeClass('active');
            link.addClass('inactive-mobile');
            link.find('.icon-ok-sign').removeClass('icon-ok-sign').addClass('icon-minus-sign');
            link.find('.hover').removeClass('icon-minus-sign').addClass('icon-plus-sign');
        }
     }

    /* ---------------------------
     |
     | - URL Parsing
     |  
     ---------------------------- */

    function parseURL(callURL, link, signal){

        var url = new Uri(callURL),
            termtax = link.data('taxonomy'),
            termslug = link.data('termslug'),
            paramValues = url.getQueryParamValues(termtax),
            parametersExist = false;

        if ( paramValues.length !== 0 ) {
            parametersExist = true;
        }

        if ( signal == 'add' ) {

            // # Add Terms to filtering

            // If the taxonomy doesn't exist in the query then add it with the links term
            if ( parametersExist === false ) {
                url.addQueryParam(termtax, termslug);
                callURL = url.toString();
            } else {
                // If it's already there, then add the term to the existing taxonomy
                var existingValues = paramValues.toString();

                newParamValues = existingValues + ',' + termslug;

                // Clean the new parameter stack from double commas
                newParamValues = newParamValues.replace(',,', ',');

                url.replaceQueryParam(termtax, newParamValues);
            }

        } else {
            
            // # Remove Terms from filtering

            if ( parametersExist ) {

                // Get the current parameter values and return them as an array
                var paramValuesArray = paramValues.toString().split(','),
                    // Then check if the term is amongs them
                    parameterValueArrayPosition = $.inArray(termslug, paramValuesArray);

                // If it is, then remove it from the array
                if ( ~parameterValueArrayPosition ) {
                    paramValuesArray.splice( parameterValueArrayPosition, 1 );
                }

                // Return the new parameter values as a string again
                var newParamValues = paramValuesArray.toString();

                // Clean the new parameter stack from double commas
                newParamValues = newParamValues.replace(',,', ',');

                // Clean commas from beginning of parameter stack
                while(newParamValues.charAt(0) === ',') {
                    newParamValues = newParamValues.substr(1);
                }

                // If the value removed from the parameter was the last one, then remove the parameter
                if ( newParamValues.length === 0 ) {
                    url.deleteQueryParam(termtax);
                } else {
                    url.replaceQueryParam(termtax, newParamValues);
                }

            }
        }

        callURL = url.toString();
        return callURL;

    }

    /* ---------------------------
     |
     | - Term Pop Over
     |  
     ---------------------------- */

    function termPopOver() {
        if ( isTablet === false ) {
            var options = { placement: 'top', trigger: 'hover' };
            $('#filter-section .product-cat-list-wrap').find('.popover-info').popover(options);
        }
    }

    $(document.body).on('click', function() {
        if ( isTablet === false) {
            $('#filter-section .product-cat-list-wrap').find('.popover-info').popover('hide');
        }
    })

})(jQuery, this);