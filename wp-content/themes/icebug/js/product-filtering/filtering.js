(function ($, angular, window) {


	var app = angular.module('FilteringApp', ['ui.bootstrap']);

    /* --------------------------------
     |   - URL Factory
     ----------------------------------*/
	app.factory('urlFactory', function($location) {
		return {

			createUrlParams: function(objects, excludeDelete) {
				// Apply them on the URL
				for(item in objects) {
					$location.search(item, objects[item].join());
				}
				// Check if there is parameters left that is not used
				var currentParams = $location.search();
				for(param in $location.search()) {
					if( param in objects == false && jQuery.inArray(param, excludeDelete) === -1 ) {
						delete currentParams[param];
					}
				}
				$location.search(currentParams);

			},

			searchQuery: function($scope, exclude) {
				var currentTaxonomies = $location.search();
				for(taxonomy in currentTaxonomies) {

					var termArray = currentTaxonomies[taxonomy].split(',');

					for(var i = 0; i < termArray.length; i++) {
						if(taxonomy !== exclude) {
                            var termSlug = termArray[i];
                            if(taxonomy in $scope.activeTaxonomies) {
                                $scope.activeTaxonomies[taxonomy].push(termSlug);
                            } else {
                                $scope.activeTaxonomies[taxonomy] = [termSlug];
                            }
						}
					}
				}
			},

			resetURL: function() {
				$location.search({});
			}
		}
	});

	app.controller("MainCtrl", function ($scope, $location, $timeout, $http, urlFactory) {


		/* --------------------------------
		|   - Fetch Products and Terms
		----------------------------------*/
		$scope.products = window.allProducts;
		$scope.productTypeTerms = window.productTypeTerms;
		$scope.productGroupTerms = window.productGroupTerms;
		$scope.productCategoryTerms = window.productCategoryTerms;
		$scope.productTechGripTerms = window.productTechGripTerms;
		$scope.productTechMembraneTerms = window.productTechMembraneTerms;
		$scope.productColdTerms = window.productColdTerms;

        $scope.activeTaxonomies = [];
			$scope.textSearch = $location.search().search ? $location.search().search : '';

        urlFactory.searchQuery($scope, 'search');

        /* --------------------------------
         |   - Text Filter
         ----------------------------------*/
		$scope.textFilter = function() {
			if($scope.textSearch.length > 0) {
				$location.search('search', $scope.textSearch);
			} else {
				$location.search('search', null);
			}
		}

        /* --------------------------------
         |   - If Term Has Image
         ----------------------------------*/

        $scope.termHasImage = function(term) {
            return (term.image !== undefined);
        }

        /* --------------------------------
         |   - In Taxonomy Filter
         ----------------------------------*/
        $scope.filterByTaxonomy = function(item) {

            var activeTaxonomies = 0,
                activeTerms = 0;

            for(var taxonomy in $scope.activeTaxonomies) {
                activeTaxonomies++;
                for(var key in item.terms) {
                    var term = item.terms[key];
                    if(jQuery.inArray(term.slug, $scope.activeTaxonomies[taxonomy]) !== -1) {
                        activeTerms++;
                    }
                }
            }
            if ( activeTerms >= activeTaxonomies ) {
                return item;
            }
            return false;
        }

		/* --------------------------------
		|   - Filter Button States
		----------------------------------*/

		$scope.checkActiveTerm = function(term) {
            if ( term.taxonomy in $scope.activeTaxonomies ) {
                var indexOfSlug = jQuery.inArray(term.slug, $scope.activeTaxonomies[term.taxonomy]);
                if ( indexOfSlug === -1 ) {
                    $scope.activeTaxonomies[term.taxonomy].push(term.slug);
                } else {
                    $scope.activeTaxonomies[term.taxonomy].splice(indexOfSlug, 1);
                    if($scope.activeTaxonomies[term.taxonomy].length < 1) {
                        delete $scope.activeTaxonomies[term.taxonomy];
                    }
                }
            } else {
                $scope.activeTaxonomies[term.taxonomy] = [term.slug];
            }
			urlFactory.createUrlParams($scope.activeTaxonomies, 'search');
		}

		$scope.termLinkClass = function(term) {
            var isActive = false;
            for(taxonomy in $scope.activeTaxonomies) {
                if ( jQuery.inArray(term.slug, $scope.activeTaxonomies[taxonomy]) !== -1) isActive = true;
            }
            return isActive ? 'active' : undefined;
		}

		$scope.termLinkIcon = function(term) {
            var isActive = false;
            for(taxonomy in $scope.activeTaxonomies) {
                if (jQuery.inArray(term.slug, $scope.activeTaxonomies[taxonomy]) !== -1) isActive = true;
            }
            return isActive ? 'icon-minus-sign' : 'icon-plus-sign';
		}

		$scope.termLinkHoverIcon = function(term) {
            var isActive = false;
            for(taxonomy in $scope.activeTaxonomies) {
                if ( jQuery.inArray(term.slug, $scope.activeTaxonomies[taxonomy]) !== -1) isActive = true;
            }
            return isActive ? 'icon-plus-sign' : 'icon-minus-sign';
		}

		$scope.resetFilter = function() {
			$scope.activeTaxonomies = [];
			$scope.textSearch = '';
			urlFactory.resetURL();
		}

		$scope.decodeEntities = function(str) {

			if(str && typeof str === 'string') {
				// strip script/html tags
				str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
				str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
				element.innerHTML = str;
				str = element.textContent;
				element.textContent = '';
			}

			return str;
		}

		$scope.decodeEntities = (function() {
			// this prevents any overhead from creating the object each time
			var element = document.createElement('div');

			function decodeHTMLEntities (str) {
				if(str && typeof str === 'string') {
					// strip script/html tags
					str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
					str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
					element.innerHTML = str;
					str = element.textContent;
					element.textContent = '';
				}

				return str;
			}

			return decodeHTMLEntities;
		})();

        /* --------------------------------
         |   - Mobile
         ----------------------------------*/

        $scope.mobileMenuOpen = false;
        $scope.typeTaxonomyOpen = false;
        $scope.groupTaxonomyOpen = false;
        $scope.categoryTaxonomyOpen = false;
        $scope.gripTaxonomyOpen = false;
        $scope.membraneTaxonomyOpen = false;

        $scope.openActiveTabs = function() {
            for(var taxonomy in $scope.activeTaxonomies) {
                if(taxonomy === 'product_type') {$scope.typeTaxonomyOpen = true};
                if(taxonomy === 'product_group') $scope.groupTaxonomyOpen = true;
                if(taxonomy === 'product_category') $scope.categoryTaxonomyOpen = true;
                if(taxonomy === 'product_tech_grip') $scope.gripTaxonomyOpen = true;
                if(taxonomy === 'product_tech_membrane') $scope.membraneTaxonomyOpen = true;
                if(taxonomy === 'product_tech_cold') $scope.typeTaxonomyOpen = true;
            }
        }

        $scope.openActiveTabs();

        $scope.toggleMobileMenu = function() {
            $scope.mobileMenuOpen = !$scope.mobileMenuOpen;
        }

        $scope.toggleTaxonomy = function(tax) {
            return $scope[tax] = !$scope[tax];
        }

	});
})(jQuery, angular, window);