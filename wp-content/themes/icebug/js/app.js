(function ($, window, undefined) {
    'use strict';

    var isMobile = false,
        isTablet = false;

    if ( Modernizr.mq("screen and (max-width:767px)") ) {
        isMobile = true;
    }

    if ( Modernizr.mq("screen and (max-width:1023px)") ) {
        isTablet = true;
    }

    /* --------------------------------
    |
    |   - Slider
    |
    ----------------------------------*/

    $(window).load(function () {

        $('#tabs a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');

            // To make sure that the tabs is loaded before we initialize slider
            if ($(this).attr('href') == '#tab-store-images') {
                $(".flexslider").flexslider({
                    animation: "slide"
                });
            }

        });

        $('#load-more').on('click', function (e) {
            e.preventDefault();
        });


        // Newsletter:

            // Populate countries on click
            $('#newsletter').on('click', '.btn', function (e) {


                var nl_email = $('#nl-email').val();
                if( /(.+)@(.+){2,}\.(.+){2,}/.test(nl_email) ){

                    $('#newsletter-modal').modal();

                    // valid email
                    var base_URL = $('#footer-section').data('base-url');

                    $.getJSON( base_URL + "/wp-content/themes/icebug/js/vendor/countries.json", function( data ) {
                        $('#nl-country-select').html('');
                        $.each( data , function(key, value) {
                            $('#nl-country-select')
                                .append($('<option>', { value : value.name })
                                .text(value.name));
                        });

                    });
                } else {
                    // invalid email
                    alert('Ange en giltig e-post');
                }
                e.preventDefault();
            });

            // Update form values on change
            $('#nl-country-select').change(function(){
                $('#nl-country').val($(this).val());
            });

            // Validate on modal close
            $('#newsletter-modal').on('hidden.bs.modal', function () {
                if($('#nl-country').val() != "") {
                    // Submit

                    var base_URL = $('#footer-section').data('base-url');

                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: base_URL + '/wp-admin/admin-ajax.php',
                        data: {
                            action: 'newsletterAjaxFunction',
                            email: $('#nl-email').val(),
                            code: $('#nl-country').val()
                        },
                        success: function(data, textStatus, XMLHttpRequest){

                            if(data.status == 0 ) {
                                alert(data.msg);
                            } else {
                                $('#thanks-newsletter-modal').modal();
                                $('#nl-email').val('');
                            }
                        },
                        error: function(MLHttpRequest, textStatus, errorThrown){
                            alert(errorThrown);
                        }
                    });



                } else {
                    // NO Country - fail hard
                    alert('Du måste välja en e-post för att kunna skriva upp dig. Din anmälan har inte sparats');
                }
            })




    });

    $(document).bind('live-edit-updated', function (event, status) {
        jQuery(".flexslider").flexslider({
            animation: "slide"
        });
    });


    $('.product-categories-select').on('change', function () {
        if ($(this).val()) {
            $('#categories-form').submit();
        }
    })

    /* ---------------------------
     |
     | - jPanel Menu
     |
     ---------------------------- */

    if ( isMobile === true ) {

        var jPM = $.jPanelMenu({
            menu: '#mobile-menu',
            trigger: '.mobile-show-menu-btn',
            animated: false,
            afterOpen: function(){
                $('body').addClass('open-jpanel')
            },
            afterClose: function(){
                $('body').removeClass('open-jpanel')
            }
        });

        jPM.on();

    }


    /* ---------------------------
     |
     | - Spin
     |
     ---------------------------- */

    window.addLoader = function(el) {

        var opts = {
            lines: 13, // The number of lines to draw
            length: 10, // The length of each line
            width: 1, // The line thickness
            radius: 10, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            direction: 1, // 1: clockwise, -1: counterclockwise
            color: '#000', // #rgb or #rrggbb or array of colors
            speed: 1, // Rounds per second
            trail: 60, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: true, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: 'auto', // Top position relative to parent in px
            left: 'auto' // Left position relative to parent in px
        };

        var jsEl = el.substring(1);
        var target = document.getElementById(jsEl);
        $(el).animate({
            opacity : 0.3
        }, function() {
            var spinner = new Spinner(opts).spin(target);
        })

    }

    window.removeLoader = function(el) {

        $(el).find('.spinner').remove();

        $(el).animate({
            opacity: 1
        })
    }

    /* ---------------------------
     |
     | - Testimonials Form
     |
     ---------------------------- */

    // Validation

    if ($(document.body).hasClass('page-template-templatespage-write-testimonials-php')) {

        var testimonialMaxChars;
        testimonialMaxChars = 150;


        $('#testimonial-form').validate({
            rules: {
                testimonial_product: 'required',
                testimonial_name: 'required',
                testimonial_email: {
                    required: true,
                    email: true
                },
                testimonial_location: {
                    required: true
                },
                testimonial_rating: {
                    required: true
                },

                testimonial_text: {
                    required: true,
                    maxlength: 150,
                    minlength: 10
                },
                accept_terms: 'required'
            },
            messages: {
                testimonial_product: 'Du måste välja en produkt',
                testimonial_name: 'Du måste ange ett namn',
                testimonial_email: {
                    required: 'Du måste ange en epostadresss',
                    email: 'Din epostadress är i felaktigt format'
                },
                testimonial_location: {
                    required: 'Din epostadress är i felaktigt format'
                },
                testimonial_rating: {
                    required: 'Du måste ange ett betyg'
                },

                testimonial_text: {
                    required: 'Du måste skriva en recension',
                    maxlength: 'Recensionen får max vara ' + testimonialMaxChars + ' tecken!',
                    minlength: 'Recensionen måste minst vara 10 tecken lång'
                },
                accept_terms: {
                    required: 'Du måste godkänna avtalet'
                }
            },
            errorPlacement: function(error, element) {
                if ( element.is(':checkbox')) {
                    element.parent('label').addClass('checkbox-error');
                }
            }
        });

        $('#testimonial-form').find('.checkbox').find('input').on('change', function() {
            if ( $(this).is(':checked') ) {
                $(this).parent().removeClass('checkbox-error');
            }
        });

        // Ratings

        var ratingClicked = false;

        $('.testimonial-rating-form .rating').on('mouseenter touchstart', function () {
            if (!ratingClicked) {
                $(this).prevUntil('.rating-placeholder').addClass('active-rating');
                $(this).addClass('active-rating');
                $(this).nextAll().removeClass('active-rating');
            }

            // Hook up rating to rating input field
            var rating = $('.testimonial-rating-form').find('.active-rating').length;
            $('#testimonial-rating').val(rating);
        });

        $('.testimonial-rating-form .rating').on('click', function () {
            ratingClicked = true;
            setTimeout(function () {
                ratingClicked = false;
            }, 1000);
        });

    }

    /* ---------------------------
     |
     | - Mobile Side Menu
     |
     ---------------------------- */

    if ( isTablet ) {

        $('#dl-menu').find('.children').removeClass('children').addClass('dl-submenu');

        $('#dl-menu .dl-submenu').each(function(index, submenu) {
            var submenu = $(submenu),
                mainLink = submenu.prev();
            submenu.first().prepend('<li><a title="' + $(mainLink).text() + '" href="' + $(mainLink).attr('href') + '">'+ $(mainLink).text() +'</a></li>');
        })

        $(function() {
            $( '#dl-menu' ).dlmenu({
                animationClasses : { classin : 'dl-animate-in-4', classout : 'dl-animate-out-4' }
            });
        });
    }

    /* ---------------------------
     |
     | - Tablet Hide Slogan on Search
     |
     ---------------------------- */

    $('#top-section').find('#search').on('focus', function() {
        $('#top-section').find('p')
            .css({
                position: 'absolute',
                opacity: 0
            })
    })

    $('#top-section').find('#search').on('blur', function() {
        $('#top-section').find('p')
            .css({
                position: 'static',
                opacity: 1
            })
    });

    /* ---------------------------
     |
     | - Read Testimonials
     |
     ---------------------------- */


    $('.read-testimonial').find('a.scroll').on('click', function(e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop : ($('.testemonial-slider-module').offset().top - 100)
        })

    });

    /* ---------------------------
     |
     | - Testimonial Filtering
     |
     ---------------------------- */

    $('select.product-categories-select').on('change', function() {
       if ( $(this).attr('value') === '' ) {
           window.location.href = $(this).data('testimonialurl')
       }
    });

	/* --------------------------------
	|
	|   - Fancybox
	|
	----------------------------------*/

	$.each($('.single-post').find('.blog-post').find('a'), function(index, link) {
		if ( $(link).find('img').length > 0 ) {
			$(link).attr('rel', 'fancybox-thumb');
		}

	});

	$('a[rel=fancybox-thumb]').fancybox({
		helpers:  {
			thumbs : {
				width: 50,
				height: 50
			}
		}
	});



})(jQuery, this);
