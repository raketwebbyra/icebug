<?php get_header(); ?>
<?php get_template_part('/template-parts/mobile/mobile-sidebar'); ?>
<?php
$news_icl = icl_object_id( get_page_by_path('nyheter')->ID , 'page', true);
$post = get_post($news_icl);
setup_postdata($post);
$paged = get_query_var('paged');?>

<?php get_template_part('/template-parts/breadcrumbs'); ?>
<section id="main-section" role="main">
    <div class="container">
        <div class="row">

            <aside id="sidebar" class="span3 hide-for-mobile">

                <?php

                $news_icl = icl_object_id( get_page_by_path('nyheter')->ID , 'page', true);
                $categories = get_categories();
                $current_cat = get_the_category();
                // Output sub-menu

                $link_class = '';
                if ( $current_cat == false ) {
	                $link_class = 'current_page_item';
                }

                echo '<ul class="sub-menu">';
                echo '<li class="'.$link_class.'"><a href="'. get_permalink($news_icl) . '">' . _x('Alla nyheter', 'Nyheter', 'icebug') . '</a></li>';
                foreach ( $categories as $cat ) {
	                $link_class = '';
	                if ( $cat->term_id == $current_cat[0]->term_id ) {
		                $link_class = 'current_page_item';
	                }
	                echo '<li class="'.$link_class.'"><a href="' . get_term_link($cat) . '">' . $cat->name . '</a></li>';
                }
                echo '</ul>';

                // Ad loop
                include(get_stylesheet_directory().'/template-parts/ad-loop.php'); ?>

            </aside>

            <div id="page-content" class="span9">


                    <?php if ( !$paged ) : ?>

                    <article <?php post_class('news-intro') ?> id="post-<?php the_ID(); ?>">
                        <h1 class="page-title"><?php the_title(); ?></h1>
                        <?php the_content(); ?>
                    </article>

                    <?php else : ?>

                        <h1 class="page-title"><?php the_title(); ?></h1>

                    <?php endif; ?>

                <?php wp_reset_postdata(); ?>

                <div class="posts">
                    <?php get_template_part('template-parts/loops/loop', 'index'); ?>
                </div>

                <?php get_template_part('template-parts/pagination'); ?>

            </div>

        </div>
    </div><!--//container-->
</section>
<?php get_footer(); ?>