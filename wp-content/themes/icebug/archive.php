<?php get_header(); ?>
<?php get_template_part('/template-parts/mobile/mobile-sidebar'); ?>
<?php get_template_part('/template-parts/breadcrumbs'); ?>
    <section id="main-section" role="main">
        <div class="container">
            <div class="row">

                <aside id="sidebar" class="span3 hide-for-mobile">

                    <?php

                    icebug_list_all_categories();

                    // Ad loop
                    include(get_stylesheet_directory().'/template-parts/ad-loop.php'); ?>

                </aside>

                <div id="page-content" class="span9">

                    <header>
                        <h1 class="page-title">
                            <?php if (is_day()) : ?>
                                <?php printf(__('Arkiv: %s'), get_the_date()); ?>
                            <?php elseif (is_month()) : ?>
                                <?php printf(__('Arkiv: %s'), get_the_date('F Y')); ?>
                            <?php elseif (is_year()) : ?>
                                <?php printf(__('Arkiv: %s'), get_the_date('Y')); ?>
                            <?php else : ?>
                                <?php single_cat_title(); ?>
                            <?php endif; ?>
                        </h1>
                    </header>

                    <div class="posts">
                        <?php get_template_part('template-parts/loops/loop', 'index'); ?>
                    </div>

                    <?php get_template_part('template-parts/pagination'); ?>

                </div>

            </div>
        </div><!--//container-->
    </section>
<?php get_footer(); ?>