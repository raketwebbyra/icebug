<?php
/*
 * Template Name: Pressida
 * */

get_header(); ?>

<!-- Breadcrumbs -->
<?php get_template_part('/template-parts/breadcrumbs'); ?>
<?php get_template_part('/template-parts/mobile/mobile-sidebar'); ?>

<section id="main-section" role="main">
    <div class="container">
        <div class="row">

            <div id="page-content" class="span12 press-content" <?php live_edit('post_title,post_content,related_products') ?>>
                <?php while (have_posts()) : the_post(); ?>
                    <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
                        
                        <?php get_template_part('template-parts/page-slider'); ?>

                        

                        <script type="text/javascript" charset="utf-8" id="mnd-script"> document.domain = /([a-z0-9-]+\.((co|org|gov)\.\w{2}|\w+))$/i.exec(location.hostname)[0];
 (function(){var s=document.createElement("script");s.type="text/javascript";s.async=true;s.src="//icebug.mynewsdesk.com/hosted_newsroom.js";var i=document.getElementsByTagName('script')[0];i.parentNode.insertBefore(s,i);})();</script>


                    </article>
                <?php endwhile; //End the loop ?>
            </div>

        </div>
    </div>
</section>
<?php get_footer(); ?>