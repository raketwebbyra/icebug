<?php while (have_posts()) : the_post(); //Start the Loop ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?>>

        <p class="small"><?php the_time(); ?> | <?php the_terms( $post->ID, 'press_category', '', ', ', '' );; ?></p>
        <p><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></p>

    </article>
<?php endwhile; //End the loop ?>