<?php
/*
    Template Name: Sida för produkt som utgått
*/
get_header(); ?>

    <!-- Breadcrumbs -->
<?php get_template_part('/template-parts/breadcrumbs'); ?>
<?php get_template_part('/template-parts/mobile/mobile-sidebar'); ?>

    <section id="main-section" role="main">
        <div class="container">
            <div class="row">

                <?php while (have_posts()) : the_post(); ?>

                <header>
                    <h1 class="page-title text-center"><?php the_title(); ?></h1>
                    <h5 class="text-center"><?php _ex('Kanske någon av dessa skor kan intressera dig istället.', 'Produkten utgått', 'icebug'); ?></h5>
                </header>

                <?php endwhile; //End the loop ?>

            </div>
        </div>

    </section>

    <?php
    $products = get_field('related_products');
    $white_bg = true;
    include(locate_template('template-parts/modules/product_slider_module.php')); ?>

<?php get_footer(); ?>