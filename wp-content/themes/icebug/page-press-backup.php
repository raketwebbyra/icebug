<?php
/*
 * Template Name: Pressida
 * */

get_header(); ?>

<!-- Breadcrumbs -->
<?php get_template_part('/template-parts/breadcrumbs'); ?>
<?php get_template_part('/template-parts/mobile/mobile-sidebar'); ?>

<section id="main-section" role="main">
    <div class="container">
        <div class="row">

            <div id="page-content" class="span12 press-content" <?php live_edit('post_title,post_content,related_products') ?>>
                <?php while (have_posts()) : the_post(); ?>
                    <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
                        
                        <?php get_template_part('template-parts/page-slider'); ?>

                        <h1 class="page-title"><?php the_title(); ?></h1>
                        <div class="content"><?php the_content(); ?></div>

                        <iframe id="press-iframe" width="100%"  height="1000px" src="http://www.mynewsdesk.com/uk/icebug/latest_news#content-area" frameborder="0"></iframe>


                    </article>
                <?php endwhile; //End the loop ?>
            </div>

        </div>
    </div>
</section>
<?php get_footer(); ?>