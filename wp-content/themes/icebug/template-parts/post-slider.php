<?php if(get_field('news_slider')):  ?>
    <div id="page-slideshow" class="slideshow flexslider">
        <ul class="slides">
            <?php while(the_repeater_field('news_slider')): ?>
                <li><img class="page-image" src="<?php echo houston_resize(get_sub_field('news_slider_image'), 640, 300, true); ?>" alt="<?php the_title(); ?>"/></li>
            <?php endwhile; ?>
        </ul>
    </div>
<?php endif; ?>
<script type="text/javascript">
    jQuery(window).load(function () {
        jQuery("#page-slideshow").flexslider({
            animation: 'slide',
            smoothHeight: true,
            directionalNav: false,
            slideshow: false
        });
    });
</script>