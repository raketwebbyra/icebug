<?php // Product Categories

/* ---------------------------
|
| - Fetch product taxonomies and terms
|
---------------------------- */

$taxonomies = array(
    'product_group',
    'product_category',
    'product_type',
);

$taxonomies_tech = array(
    'product_tech_grip',
    'product_tech_membrane',
    'product_tech_cold'
);


// Fetch terms by $taxonomies array
$terms = get_terms( $taxonomies, array( 'hide_empty' => 1, 'orderby' => 'name' ) );
$terms_tech = get_terms( $taxonomies_tech, array( 'hide_empty' => 1, 'orderby' => 'name' ) );

$terms = array_merge( $terms, $terms_tech );
$taxonomies = array_merge( $taxonomies, $taxonomies_tech );


// By default we're not filtering testimonials
$category_view = false;

//Add query args for testimonial to page
add_query_arg( 'testimonial-category' );

/* ---------------------------
|
| - Fetch Category from URL
|
---------------------------- */

if ( isset($_GET['testimonial-category']) ) {
    $testimonial_product_slug = $_GET['testimonial-category'];
    $category_view = true;
}


    /* ---------------------------
    |
    | - Testimonials Form
    |
    ---------------------------- */

?>

<div id="testimonials">

    <?php // Display Success Message

    if ( isset( $_SESSION['successful'] ) ) : ?>

      <h4><?php echo _ex('Tack för din åsikt!', 'Recensioner', 'icebug'); ?></h4>
      <?php the_field('testimonial_success_text'); ?>

      <?php

        unset( $_SESSION['successful']  );

    endif; ?>

    <div class="product-cat-pick">
        <form id="categories-form" action="<?php echo get_permalink( $post->ID ) ?>" method="GET">
            <select data-testimonialurl="<?php the_permalink() ?>" name="testimonial-category" class="product-categories-select">
                <option class="select-placeholder" value=""><?php _ex('Filtrera efter produkter i produktkategori...', 'Testimonials', 'icebug'); ?></option>

                <?php foreach ( $taxonomies as $tax ) :
                    $labels = get_taxonomy( $tax );
                    echo '<optgroup label="'.$labels->labels->name.'">';

                    $terms = get_terms($tax);

                    foreach ( $terms as $term ) {

                        if ( $category_view && $term->slug == $testimonial_product_slug ) {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }

                        echo '<option '. $selected .' value="' . $term->slug . '">' . $term->name . '</option>';
                    }

                    echo '<optgroup label="----------------------------------------">';


                endforeach; ?>


            </select>
        </form>
    </div><!--//testimonials-cat-pick-->

    <?php

    /* ---------------------------
    |
    | - Filtering
    |
    ---------------------------- */

     if ( $category_view ) {

        // Array containing testimonials from our current category
        $current_testimonials = array();

        // Counter for current testimonials
        $x = 0;

        // Fetch all testimonials
        $testimonials = get_posts( array( 'post_type' => 'testemonial', 'posts_per_page' => -1, 'suppress_filters' => true ));

        foreach ( $testimonials as $testimonial ) {

            // Fetch the testimonials product id
            $testimonial_product = get_field('testimonial_product', $testimonial->ID);
            $testimonial_product = $testimonial_product[0];

            // Fetch the product post object
            $product = get_post( $testimonial_product->ID );

            // Fetch product terms
            $product_terms = wp_get_post_terms( $product->ID, $taxonomies );

            /*  If the product is in the current category
                Move this testimonial into the current testimonials array
            */
            foreach ( $product_terms as $term ) {
                if ( $term->slug == $testimonial_product_slug ) {
                    $current_testimonials[$x] =  $testimonial->ID;
                    $x++;
                }
            }

        }

    }

    /* ---------------------------
    |
    | - Testimonials Loop
    |
    ---------------------------- */

    // Pagination
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

    //Reset wp_query
    wp_reset_query();

    $show_products = true;

    if ( $category_view && !$current_testimonials ) {
        $show_products = false;
    }

if ( $show_products ) {

    // Fetch testimonials by category
    $args = array(
        'post_type' => 'testemonial',
        'posts_per_page' => 10,
        'meta_key' => 'testimonial_language',
        'meta_value' => ICL_LANGUAGE_CODE,
        'post__in' => $current_testimonials,
        'paged' => $paged,
        'suppress_filters' => true
    );

    $wp_query = new WP_Query($args);

    if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
        // Fetch product
        $product = get_field( 'testimonial_product' );
        $product = $product[0];

        if ( !$product ) {

            // If theres no product, then use the saved image
            $product_image_attachment = wp_get_attachment_image_src( get_field('testimonial_product_image'), 'large' );
            $product_image_src = $product_image_attachment;
            $product_image_src = get_field('testimonial_product_image');

            //And the saved name
            $product_name = get_field('testimonial_product_name');

            // Set the link to product removed products page
            $icl_object_id = icl_object_id(471, 'page', true);
            $product_link = get_permalink($icl_object_id);

        } else {

            // Else fetch from product
            $product_name = get_the_title($product->ID);
            $product_images = get_field('images_product', $product->ID);
            $product_image_src = $product_images[0]['image'];

            // Product title
            $product_name = get_the_title($product->ID);

            // Set product link to default product link
            $product_link = get_permalink($product->ID);
        }


        ?>

        <div class="testimonial">

            <div class="testimonial-content span8 col">

                <h3 class="testimonial-title">"<?php echo get_field('testimonial_text'); ?>"</h3>

                <?php
                $rating = intval(get_field('testimonial_rating'));
                $max_rating_number = 5;?>

                <ul class="testimonial-rating">
                    <?php for ( $i = 0; $i < 5; $i++ ) { ?>
                        <li class="rating <?php if( $i < $rating ) echo ' active-rating' ?>">
                            <span class="icon icon-icebug"></span>
                        </li>
                    <?php } ?>
                </ul>

                <div class="testimonial-by">
                    - <?php echo get_field('testimonial_name') . ', ' . get_field('testimonial_location') . ', '; ?>
                    om <a href="<?php echo $product_link ?>" title="<?php echo $product_name ?>"><?php echo $product_name ?></a>
                </div>

            </div><!--//testimonial-content-->

            <div class="testimonial-thumb span4 col">
                <a href="<?php echo $product_link ?>" title="<?php echo $product_name; ?>">
                    <img src="<?php echo houston_resize($product_image_src, 190, false, true); ?>" alt="<?php echo $product_name; ?>" />
                </a>
            </div>

        </div><!--//testimonial-->

    <?php endwhile; ?>

    <?php get_template_part('template-parts/pagination') ?>
    <?php endif; wp_reset_query();  ?>

<?php } else {
    _ex('Det finns inga recensioner i denna produktkategorin.', 'Testimonials', 'icebug');
} ?>

</div><!--//#testimonials-->