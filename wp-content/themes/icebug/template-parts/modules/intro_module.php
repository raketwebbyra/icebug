<!-- Text module -->
<section class="module <?php echo (get_sub_field('background') == 'white') ? 'secondary' : ''; ?> border" role="main">
    <div class="container row intro-module">
        <div class="span12">
            <?php echo get_sub_field('text'); ?>
        </div>
    </div>
</section>