<!-- Slider -->
<?php if ( !is_mobile() ) : ?>
<section id="slider-module-<?php echo $module_index; ?>" class="flexslider main-slider" style="border:none">
    <ul class="slides">
        <?php while(has_sub_field('slide')): ?>
        <li style="background-color:<?php echo get_sub_field('background') ?>; ">
            <?php
			        $bg_color = get_sub_field('board_bg_color') ? hex2rgb(get_sub_field('board_bg_color')) : hex2rgb('#000');
                    $bg_color_hex = get_sub_field('board_bg_color') ? get_sub_field('board_bg_color') : '#000';
			        $text_color = get_sub_field('board_text_color') ? get_sub_field('board_text_color') : get_sub_field('board_text_color');
                    $opacity = get_sub_field('board_opacity') ? get_sub_field('board_opacity') : 0.4;

                    $image = ($src = get_sub_field('image')) ? '<img src="' . houston_resize($src, 1400, 470, true) . '" alt="" title="" />' : '';
                    $header = ($src = get_sub_field('header')) ? "<h2 style='color:{$text_color}' class='title'>{$src}</h2>" : '';
                    $text = ($src = get_sub_field('text')) ? $src : '';

                    $button = ($src = get_sub_field('button')) ? '<a style="background-color:'.$text_color.'; color: '.$bg_color_hex.'" class="btn" href="' .get_sub_field('link') . '" title="' . get_sub_field('header') . '">' . $src . ' <span class="icon icon-chevron-sign-right"></span></a>' : '';
                    $layout = get_sub_field('layout');

                    $content = '';
                        $content .= $image;
                            $content .= "<div style='background-color: {$bg_color_hex}; background-color:rgba({$bg_color[0]},{$bg_color[1]},{$bg_color[2]}, {$opacity}); color:{$text_color}' class='text {$layout}'>";
                                $content .= $header;
                                $content .= '<div class="text-content">' . $text . '</div>';
                                $content .= $button;
                            $content .= '</div>';
                    echo $content;
            ?>
        </li>
        <?php endwhile; ?>
    </ul>
    <script type="text/javascript">
        jQuery(window).load(function () {
            jQuery("#slider-module-<?php echo $module_index; ?>").flexslider();
        });
    </script>
</section>
<?php else :

    $slides = get_sub_field('slide');
    $first_slide = $slides[0];
    $image_id = $first_slide['image'];

    $thumb = houston_resize($image_id , 600, 300, true);
    
    ?>
    <section id="slider-module-<?php echo $module_index; ?>" class="main-slider" style="border:none">
        <img src="<?php echo $thumb ?>" alt=""/>
    </section>
    <?php

endif; ?>