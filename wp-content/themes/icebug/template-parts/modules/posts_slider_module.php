<?php
$posts = get_posts( array( 'post_type' => 'post', 'numberposts' => 9, 'suppress_filters' => false ) );
if ( $posts ) :

    $items_per_row = 3;
    $rows = ceil(count($posts)/3);
    $c = 0;

    if ( is_mobile() ) {
        $items_per_row = 1;
        $rows = count($products);
    } ?>

<h4 class="section-header"><?php echo get_sub_field('header') ?></h4>
<!-- Product slider module -->
<section class="slider-module module <?php echo (get_sub_field('background') == 'white') ? 'secondary' : ''; ?> border">
    <div class="container news-slider-module">
        <div class="carousel flexslider">
            <ul class="slides">
                <?php for($i = 0; $i < $rows; $i++ ): ?>
                    <li class="blog-post">
                        <div class="row">
                            <?php
                            $items_per_row = 3;
                            if ( is_mobile() ) {
                                $items_per_row = 1;
                            }
                            for($y = 0; $y < $items_per_row; $y++ ):
                                $post = $posts[$c]; setup_postdata($post); ?>

                                <div class="span4">
                                    <?php
                                    $post_images = get_field('news_slider');
                                    $post_image_id = $post_images[0]['news_slider_image'];
                                    $post_thumb = houston_resize( $post_image_id, 290, 185, true );

                                    if ( $post_thumb ) :?>
                                    <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
                                        <img src="<?php echo $post_thumb ?>" alt="<?php the_title() ?>"/>
                                    </a>

                                    <?php endif;?>
                                    <h4 class="title"><?php the_title(); ?></h4>
                                    <div class="meta">
                                        <i class="icon icon-time"></i><?php the_time('F j Y') ?>
                                        <?php $post_categories = get_the_category();
                                        if ( $post_categories ) : ?>
                                            <i class="icon icon-folder-open"></i>

                                            <ul class="cats">
                                                <?php $x = 0; foreach ( $post_categories as $cat ) : $x++; ?>
                                                    <li>
                                                        <a href="<?php echo get_category_link( $cat->term_id ) ?>" title="<?php echo $cat->cat_name ?>"><?php echo $cat->cat_name ?></a><?php if ($x != count($post_categories)) echo ', '; ?>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        <?php endif; ?>
                                    </div>
                                    <p>
                                        <?php echo excerpt(18) ?>
                                    </p>
                                </div>
                            <?php
                                if($c == (count($posts)-1)) {
                                    break;
                                }
                                wp_reset_postdata(); $c++; endfor; ?>
                        </div>
                    </li>
                <?php endfor; ?>
            </ul>
            <script type="text/javascript">
                jQuery(window).load(function () {
                    jQuery(".carousel").flexslider({
                        animation: 'slide',
                        slideshow: false
                    });
                });
            </script>
        </div>
    </div>
</section>

<?php endif; ?>