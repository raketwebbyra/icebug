<?php
    // Get context
    if(get_sub_field('products')) {
        $products = get_sub_field('products');
    }

    // Set up deps for rows
    $items_per_row = 3;
    $rows = ceil(count($products)/3);
    if ( is_mobile() ) {
        $items_per_row = 1;
        $rows = count($products);
    }

if ( get_sub_field('products') ) :
?>
<!-- Product slider module -->
<section class="slider-module module <?php echo (get_sub_field('background') == 'white') ? 'secondary' : ''; echo ( $white_bg ) ? 'secondary' : ''; ?> border">
    <div class="container product-slider-module">
        <div id="product-carousel-<?php echo $module_index; ?>" class="carousel flexslider">
            <ul class="slides">
                <?php $x = 0; for($i = 0; $i < $rows; $i++ ): ?>
                    <li class="">
                        <div class="row">
                            <?php for($y = 0; $y < $items_per_row; $y++ ): ?>
                                <div class="span4">
                                    <?php
                                        $product_images = get_field('images_product',$products[$x]->ID);
                                        $src = $product_images[0]['image'];
                                        $image = ($src) ? houston_resize($src, 300, 200, true) : get_stylesheet_directory_uri().'/images/default.png';
                                    ?>
                                    <a href="<?php echo get_permalink($products[$x]->ID) ?>" title="">
                                        <img alt="" src="<?php echo $image; ?>" />
                                    </a>
                                    <h4 class="title"><?php echo $products[$x]->post_title ?></h4>
                                    <p>
                                       <?php echo wp_trim_words($products[$x]->post_content,15); ?>
                                        <a href="<?php echo get_permalink($products[$x]->ID) ?>" title=""><?php _ex('Läs mer','Produktslider','icebug'); ?> <i class="icon-caret-right"></i></a>
                                    </p>
                                </div>
                                <?php
                                    if($x == (count($products)-1)) {
                                        break;
                                    }
                                ?>
                            <?php $x++; endfor; ?>
                        </div>
                    </li>
                <?php endfor; ?>
            </ul>
            <script type="text/javascript">
                jQuery(window).load(function () {
                    jQuery("#product-carousel-<?php echo $module_index; ?>").flexslider({
                        animation: 'slide',
                        slideshow: false
                    });
                });
            </script>
        </div>
    </div>
</section>
<?php endif; ?>