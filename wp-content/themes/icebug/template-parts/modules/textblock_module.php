<!-- Text module -->
<section class="module <?php echo (get_sub_field('background') == 'white') ? 'secondary' : ''; ?> border" role="main">
    <div class="container row text-module">
        <div class="span12">
            <?php
            if(get_sub_field('image'))
                echo '<img src="'.houston_resize(get_sub_field('image'),9999,9999).'" alt=""/>';
            echo get_sub_field('text');

            if(get_sub_field('link'))
                echo '<a class="btn" href="' . get_sub_field('link') . '" title="">'._x('Läs mer', 'Text & bildblock','icebug').' <span class="icon icon-chevron-sign-right"></span></a>';
            ?>
        </div>
    </div>
</section>