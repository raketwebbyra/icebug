<?php
// Get context
if(get_sub_field('testemonials')) :
    $testemonials = get_sub_field('testemonials'); ?>

<?php if ( !is_mobile() ) : ?>
<!-- Testemonial module -->
<section class="slider-module module <?php echo (get_sub_field('background') == 'white') ? 'secondary' : ''; ?> border">
    <div class="container testemonial-slider-module">
        <div class="row">
            <div class="span12">
                <div id="testemonial-slider-<?php echo $module_index; ?>" class="flexslider">
                    <ul class="slides">
                        <?php
                        // Save current product ID in var
                        // Foreach testimonial
                        foreach ( $testemonials as $testimonial ) :
                            // Setup testimonial as global post object
                            $post = $testimonial;
                            setup_postdata( $post );
                            $product = get_field('testimonial_product');
                            $product = $product[0];
                            $product_images = get_field('images_product',$product->ID);
                            $image = $product_images[0]['image'];
                            ?>
                            <li class="">

                                <div class="span6">
                                    <div class="quote testimonial-title"><?php echo wpautop('“' . get_field( 'testimonial_text') . '”'); ?></div>
                                    <?php
                                    $rating = intval(get_field('testimonial_rating'));
                                    $max_rating_number = 5;
                                    ?>

                                    <ul class="testimonial-rating">
                                        <?php for ( $i = 0; $i < 5; $i++ ) { ?>
                                            <li class="rating <?php if( $i < $rating ) echo ' active-rating' ?>">
                                                <span class="icon icon-icebug"></span>
                                            </li>
                                        <?php } ?>
                                    </ul>

                                    <div class="cite testimonial-by">
                                        <?php
                                        $testimonial_name = get_field( 'testimonial_name' );
                                        echo '-  ' . $testimonial_name . '  om <a href="' . $product->guid . '">' . $product->post_title . '</a>'; ?>
                                    </div>

                                    <?php
                                    /* Se all testimonials link
                                    Fetch language friendly post object */
                                    $icl_object_id = icl_object_id(92, 'page', true); ?>
                                    <a class="mobile-btn btn" href="<?php echo get_permalink( $icl_object_id ) ?>" title="<?php _ex('Fler recensioner', 'Enskild produkt', 'icebug') ?>"><?php _ex('Fler recensioner', 'Enskild produkt', 'icebug') ?> <span class="icon icon-chevron-sign-right"></span></a>

                                    <?php
                                    /* Write testimonial about this product link
                                    Fetch language friendly post object */
                                    $write_icl_object_id = icl_object_id( 181, 'page', true );
                                    $write_testimonial = get_permalink($write_icl_object_id) ?>
                                    <a class="mobile-btn btn" href="<?php echo $write_testimonial; ?> " title="<?php _ex('Skriv recension', 'Enskild produkt', 'icebug') ?>"><?php _ex('Skriv recension', 'Enskild produkt', 'icebug') ?> <span class="icon icon-edit"></span></a>
                                </div>

                                <?php if( $image ) : ?>
                                <div class="span6 testimonial-slider-product-image hide-for-mobile">
                                    <a href="<?php echo get_permalink($product->ID) ?>" title="<?php echo get_the_title($product->ID); ?>">
                                        <img src="<?php echo houston_resize($image, 510, false, true ) ?>" alt="<?php echo $product->post_title ?>"/>
                                    </a>
                                </div>
                                <?php endif; ?>
                            </li>

                        <?php endforeach; wp_reset_postdata(); // end foreach testimonial, reset post data ?>
                    </ul>
                    <script type="text/javascript">
                        jQuery(window).load(function () {
                            jQuery("#testemonial-slider-<?php echo $module_index; ?>").flexslider({
                                animation: 'fade'
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</section>

<?php else :

    $testimonial = $testemonials[0];
    $post = $testimonial;
    setup_postdata( $post );
    $product = get_field('testimonial_product');
    $product = $product[0];
    $product_images = get_field('images_product',$product->ID);
    $image = $product_images[0]['image']; ?>

<section class="slider-module module <?php echo (get_sub_field('background') == 'white') ? 'secondary' : ''; ?> border">
    <div class="container testemonial-slider-module">
        <div class="row">
            <div class="span12">

            <div class="span6">
                <div class="quote testimonial-title"><?php echo wpautop('“' . get_field( 'testimonial_text') . '”'); ?></div>
                <?php
                $rating = intval(get_field('testimonial_rating'));
                $max_rating_number = 5;
                ?>

                <ul class="testimonial-rating">
                    <?php for ( $i = 0; $i < 5; $i++ ) { ?>
                        <li class="rating <?php if( $i < $rating ) echo ' active-rating' ?>">
                            <span class="icon icon-icebug"></span>
                        </li>
                    <?php } ?>
                </ul>

                <div class="cite testimonial-by">
                    <?php
                    $testimonial_name = get_field( 'testimonial_name' );
                    echo '-  ' . $testimonial_name . '  om <a href="' . $product->guid . '">' . $product->post_title . '</a>'; ?>
                </div>

                <?php
                /* Se all testimonials link
                Fetch language friendly post object */
                $icl_object_id = icl_object_id(92, 'page', true); ?>
                <a class="mobile-btn btn" href="<?php echo get_permalink( $icl_object_id ) ?>" title="<?php _ex('Fler recensioner', 'Enskild produkt', 'icebug') ?>"><?php _ex('Fler recensioner', 'Enskild produkt', 'icebug') ?> <span class="icon icon-chevron-sign-right"></span></a>

                <?php
                /* Write testimonial about this product link
                Fetch language friendly post object */
                $write_icl_object_id = icl_object_id( 181, 'page', true );
                $write_testimonial = get_permalink($write_icl_object_id) ?>
                <a class="mobile-btn btn" href="<?php echo $write_testimonial; ?> " title="<?php _ex('Skriv recension', 'Enskild produkt', 'icebug') ?>"><?php _ex('Skriv recension', 'Enskild produkt', 'icebug') ?> <span class="icon icon-edit"></span></a>
            </div>

            <?php if( $image ) : ?>
                <div class="span6 testimonial-slider-product-image hide-for-mobile">
                    <a href="<?php echo get_permalink($product->ID) ?>" title="<?php echo get_the_title($product->ID); ?>">
                        <img src="<?php echo houston_resize($image, 510, false, true ) ?>" alt="<?php echo $product->post_title ?>"/>
                    </a>
                </div>
            <?php endif; ?>

            </div>
        </div>
    </div>
</section>

<?php wp_reset_postdata(); endif; endif; ?>