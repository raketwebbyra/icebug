<!-- Modal -->
<div class="modal fade" id="newsletter-modal" tabindex="-1" role="dialog" aria-labelledby="newsletter-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p><?php _ex('Vilket land bor du i?', 'Newsletter Dialogruta', 'icebug'); ?></p>
                <select name="nl-country-select" id="nl-country-select" style="width:88%;float:left;">
                    <option value="">Laddar...</option>
                </select>
                <button type="button" class="btn large" data-dismiss="modal" style="width:10%;float:right; padding:7px 15px;"><?php _ex('OK', 'Shop Dialogruta', 'icebug'); ?></button>
                <div class="clearfix"></div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="thanks-newsletter-modal" tabindex="-1" role="dialog" aria-labelledby="newsletter-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p><strong>Sweet!</strong> Nu prenumererar du på vårt nyhetsbrev. Vi hör av oss snart.</p>
                <button type="button" class="btn large" data-dismiss="modal" style="width:30%;float:none; margin:0 auto; padding:7px 15px;"><?php _ex('OK', 'Shop Dialogruta', 'icebug'); ?></button>
                <div class="clearfix"></div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->