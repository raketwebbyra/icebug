<?php
$thresixty_images = false;
if (the_repeater_field('360_images_product')):
    $thresixty_images = get_field('360_images_product'); ?>
    <script src="<?= bloginfo('template_url');?>/js/j360.js"></script>
    <script src="<?= bloginfo('template_url');?>/js/imagesloaded.pkgd.min.js"></script>
<?php endif; ?>

<?php if(get_field('images_product')): ?>
    <section id="product-images-section" class="module secondary border">
        <div class="container">
            <div class="row">
                <div class="span12" <?php live_edit('images_product'); ?>>
                    <div class="genders">
                    <?php
                        $genders = wp_get_post_terms($post->ID, 'product_group');
                        foreach($genders as $gender) {
                            echo '<img src="'.houston_resize( get_field('icon_tax', 'product_group_' . $gender->term_id),40,40 ).'" alt="" title="" />';
                        }
                    ?>
                    </div>
                    <section id="product-slider" class="flexslider" style="border:none">
                        <ul class="slides">
                            <?php if ($thresixty_images):
                                ?>
                                 <li>
                                     <?php if (is_mobile()): ?>
                                         <img src="<?= houston_resize($thresixty_images[0]['image'], false, 450, true); ?>" />
                                     <?php else: ?>
                                         <div id="360-product" style="height:450px; margin:0;overflow: hidden;left:0;top:0; width: 100%; cursor:move;"></div>
                                         <p class="icon-360">360<i class="icon-undo"></i></p>
                                     <?php endif;?>
                                 </li>
                            <?php
                            endif;
                            while(the_repeater_field('images_product')): ?>
                                <li>
                                    <?php if(get_sub_field('video')): ?>
                                        <?php global $content_width; $content_width = 930; ?>
                                        <?php echo do_shortcode('[video height="450px 100%" src="'.get_sub_field('video').'"]'); ?>
                                    <?php else: ?>
                                        <img src="<?php echo houston_resize(get_sub_field('image'), false, 450 , false); ?>" />
                                    <?php endif; ?>
                                </li>
                            <?php endwhile; ?>
                        </ul>

                        <script type="text/javascript">
                            jQuery(window).load(function () {

                                jQuery("#product-slider").flexslider({
                                    smoothHeight:true,
                                    controlNav: false,
                                    directionNav: true,
                                    pauseOnHover: true,
                                    slideshow: false,
                                    before: function() {
                                        jQuery(".mejs-pause").trigger('click');
                                    }
                                });
                                <?php if (!is_mobile() && $thresixty_images): ?>

                                addLoader('#product-slider');

                                jQuery.ajax({
                                    type:"POST",
                                    url: "<?= bloginfo('home');?>/wp-admin/admin-ajax.php",
                                    data: {
                                        action: 'get_360_images',
                                        id: <?= $post->ID; ?>
                                    },
                                    success:function(data){

                                        jQuery('#360-product').html(data);

                                        jQuery('#360-product img').imagesLoaded( function() {
                                        removeLoader('#product-slider');
                                        jQuery('#360-product img').imagesLoaded().done( function( instance ) {
                                            jQuery('#360-product').j360({
                                                width_step:(900/<?= count(get_field('360_images_product'));?>)
                                            }).css('margin', 0);
                                        });

                                        });
                                    }
                                });
                                <?php endif;?>
                            });

                        </script>

                        <style>
                            #view_overlay {
                                top:0 !important;
                                left:0 !important;
                            }
                        </style>

                    </section>
                </div>
            </div>
        </div>
    </section>

    <section id="product-images-thumbnails" class="module secondary">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <section id="product-slider-nav" class="flexslider">
                        <ul class="slides">
                            <?php
                            if($thresixty_images): ?>
                                <li>
                                    <img src="<?= houston_resize($thresixty_images[0]['image'], 190, 150, true); ?>" />
                                    <?= (!is_mobile()) ? '<p class="icon-360">360<i class="icon-undo"></i></p>' : '' ; ?>
                                </li>

                                <?php endif;
                            $number_of_images = count( get_field('images_product') );
                            while(the_repeater_field('images_product')): ?>
                                <li<?php if ( $number_of_images < 2 ) echo ' class="pull-left"' ?>>
                                    <?php if(get_sub_field('video')): ?>
                                        <img class="video-icon" src="<?php bloginfo("template_url"); ?>/images/video-play.png" width="28px" height="28px" alt=""/>
                                    <?php endif; ?>
                                    <img class="thumb" src="<?php echo houston_resize(get_sub_field('image'), 190, 120, true); ?>" />
                                </li>
                            <?php endwhile; ?>
                        </ul>
                        <script type="text/javascript">
                            jQuery(window).load(function () {
                                jQuery("#product-slider-nav").flexslider({
                                    animation: "slide",
                                    controlNav: false,
                                    directionNav: true,
                                    animationLoop: false,
                                    slideshow: false,
                                    itemWidth: 210,
                                    itemMargin: 5,
                                    asNavFor: '#product-slider'
                                });
                            });
                        </script>
                    </section>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>