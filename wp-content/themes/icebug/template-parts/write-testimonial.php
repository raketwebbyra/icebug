<?php // Display error messages
if ( isset( $error_list ) && $error_list != '' ) : ?>

    <ul class="error-list">
        <?php foreach ( $error_list as $error ) {
            echo '<li>' . $error . '</li>';
        } ?>
    </ul>

<?php endif;

// Allow queries for passing product ID's in to the form
add_query_arg( 'product-id' );  ?>

<form action="" id="testimonial-form" method="post">

    <div class="row">

        <div class="testimonial-form-right-col span6">

            <?php
            // Fetch products from current language
            $products = get_posts( array('post_type' => 'product', 'posts_per_page' => -1, 'suppress_filters' => false, 'orderby' => 'title', 'order' => 'asc') ); ?>

            <select class="required" name="testimonial_product" id="testimonial-product-select">
                <option value=""><?php _ex('Välj sko...', 'Recension Formulär', 'icebug'); ?></option>
                <option value="0"><?php _ex('Vet ej.', 'Recension Formulär', 'icebug'); ?></option>
                <?php foreach ( $products as $product ) :
                    if ( isset($_GET['product-id']) ) {
                        $current_product = $_GET['product-id'];
                    } ?>
                    <option <?php if ( $product->ID == $current_product && isset($current_product) ) { echo ' selected="selected" '; } ?> value="<?php echo $product->ID ?>"><?php echo $product->post_title ?></option>
                <?php endforeach; ?>
            </select>

            <input type="text" value="<?php if ( isset( $_POST['testimonial_name'] ) ) echo $_POST['testimonial_name']; ?>" class="required" name="testimonial_name" placeholder="<?php _ex('Namn:', 'Recension Formulär', 'icebug') ?>" />

            <input type="text" value="<?php if ( isset( $_POST['testimonial_email'] ) ) echo $_POST['testimonial_email']; ?>" class="required" name="testimonial_email" placeholder="<?php _ex('Epost:', 'Recension Formulär', 'icebug') ?>*"/>

            <input type="text" value="<?php if ( isset( $_POST['testimonial_location'] ) ) echo $_POST['testimonial_location']; ?>" class="required" name="testimonial_location" placeholder="<?php _ex('Ort:', 'Recension Formulär', 'icebug') ?> "/>

        </div>

        <div class="span6 testimonial-form-left-col">

            <ul class="testimonial-rating testimonial-rating-form">

                <li class="rating-placeholder">
                    <?php _ex('Betyg: ', 'Recension Formulär', 'icebug'); ?>
                </li>

                <li class="rating form-rating active-rating">
                    <span class="icon icon-icebug"></span>
                </li>
                <li class="rating form-rating active-rating">
                    <span class="icon icon-icebug"></span>
                </li>
                <li class="rating form-rating active-rating">
                    <span class="icon icon-icebug"></span>
                </li>
                <li class="rating form-rating active-rating">
                    <span class="icon icon-icebug"></span>
                </li>
                <li class="rating form-rating active-rating">
                    <span class="icon icon-icebug"></span>
                </li>
            </ul>

            <input type="hidden" id="testimonial-rating" class="required" class="testimonial_rating" name="testimonial_rating" value="5"/>

            <textarea placeholder="<?php _ex('Recension:', 'Recension Formulär', 'icebug') ?>" value="" class="required" name="testimonial_text" id="testimonial-text-textarea" cols="30" rows="7"><?php if ( isset( $_POST['testimonial_text'] ) ) echo $_POST['testimonial_text']; ?></textarea>

        </div>

        <label class="checkbox">
            <input name="accept_terms" type="checkbox" value="accept"> <small><?php _ex('Jag godkänner att min recension kan komma att användas i marknadsföringssyfte.', 'Recension Formulär', 'icebug'); ?></small>
        </label>

    </div>

    <div class="row">

        <div class="span6 form-instruction">
            <b><?php _ex('* = Kommer ej att visas', 'Recension Formulär', 'icebug'); ?></b>
        </div>

        <input type="hidden" name="testimonial_language" value="<?php echo ICL_LANGUAGE_CODE; ?>"/>

        <div class="span6">
            <?php wp_nonce_field( 'post_nonce', 'post_nonce_field' ); ?>
            <input type="hidden" name="submitted" id="submitted" value="true" />
            <button type="submit" class="btn submitBtn"><?php _ex('Skicka', 'Recension Formulär', 'icebug'); ?> <span class="icon icon-envelope"></span></button>
        </div>

    </div>

</form>