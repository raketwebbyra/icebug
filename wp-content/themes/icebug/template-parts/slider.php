<script type="text/javascript">
    jQuery(window).load(function () {
        jQuery("#featured").flexslider({
            animation: "slide",
            smoothHeight: true
        });
    });
</script>

<section id="featured" class="flexslider" style="border:none">
    <ul class="slides">
        <?php
        $source = (is_post_type_archive()) ? 'options' : false;
        $slides = get_field('slides_slider',$source);
        if(count($slides) > 0):
            foreach($slides as $slide) {
                while(has_sub_field("layout_slider",$slide->ID)):
                    echo '<li>';
                    if(get_row_layout() == "image_text"){

                        $attr = array(
                            'image' => houston_resize(get_sub_field('image_slide'),666,340,true),
                            'type' => get_sub_field('type_slide'),
                            'header' => get_sub_field('header_slide'),
                            'price' => get_sub_field('price_slide'),
                            'text' => get_sub_field('text_slide'),
                            'small_text' => get_sub_field('small_text_slide'),
                            'link_text' => get_sub_field('link_text_slide'),
                            'link' => get_sub_field('link_slide')

                        );
                        get_element('slide-text',$attr);
                        unset($attr);

                    } elseif(get_row_layout() == "image") {
                        echo '<a href="'.get_sub_field('link_slide').'" alt="'.get_the_title().'" title="'.get_the_title().'"><img src="'.houston_resize(get_sub_field('image_slide'),1004,340,true).'" height="340" alt="'.get_the_title().'"/></a>';
                    } else {
                        echo __('Vi har för närvarande inga aktiva erbjudanden');
                    }
                    echo '</li>';
                endwhile;
            }
        else:
            echo '<li>' . __('Vi har för närvarande inga aktiva erbjudanden') . '</li>';
        endif;
        ?>
    </ul>
</section>