<?php if(get_field('images_page')):  ?>
<div id="page-slideshow" class="slideshow flexslider">
    <ul class="slides">
    <?php while(the_repeater_field('images_page')): ?>
        <li><img class="page-image" src="<?php echo houston_resize(get_sub_field('image'), 530, false, true); ?>" alt="<?php the_title(); ?>"/></li>
    <?php endwhile; ?>
    </ul>
</div>
<?php endif; ?>

<script type="text/javascript">
    jQuery(window).load(function () {
        jQuery("#page-slideshow").flexslider({
            animation: 'slide',
            smoothHeight: true,
            directionalNav: false,
            slideshow: false
        });
    });
</script>

<?php
$page_main_image_id = get_field('image_page');
if( $page_main_image_id ) {
    $image = houston_resize( $page_main_image_id, 645, false, true  ); ?>
    <div class="page-main-image">
        <img src="<?php echo $image ?>" alt="<?php the_title(); ?>"/>
    </div><!--//page-main-image-->

<?php } ?>