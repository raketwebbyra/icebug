<?php

if(have_posts()): while (have_posts()) : the_post(); ?>

    <article <?php post_class('blog-post') ?> id="post-<?php the_ID(); ?>">

        <header>

            <?php $post_image = get_field('post_main_image');
            $slider_images = get_field('news_slider');
            $image = houston_resize( $post_image, 640, 300, true ); ?>

            <?php if ( $image && !$slider_images ) : ?>

                <a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
                    <img class="post-image" src="<?php echo $image; ?>" alt=""/>
                </a>

            <?php endif; ?>

            <h1 class="page-title"><?php the_title(); ?></h1>

            <div class="meta">
                <i class="icon icon-time"></i><?php the_time('j F Y') ?>
                <?php $post_categories = get_the_category();
                if ( $post_categories ) : ?>
                    <i class="icon icon-folder-open"></i>

                    <ul class="cats">
                        <?php $x = 0; foreach ( $post_categories as $cat ) : $x++; ?>
                            <li>
                                <a href="<?php echo get_category_link( $cat->term_id ) ?>" title="<?php echo $cat->cat_name ?>"><?php echo $cat->cat_name ?></a><?php if ($x != count($post_categories)) echo ', '; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

                <div class="fb-like-box">
                    <div class="fb-like" data-href="<?php the_permalink(); ?>" data-width="450" data-layout="button_count" data-show-faces="true" data-send="false"></div>
                </div>


            </div>
        </header>

        <?php the_content(); ?>

    </article>

<?php endwhile; endif;