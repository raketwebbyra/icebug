<?php

if(have_posts()): while (have_posts()) : the_post(); ?>

    <article <?php post_class('blog-post') ?> id="post-<?php the_ID(); ?>" <?php live_edit('post_title, post_content, news_slider') ?>>


        <?php

        $post_images = get_field('news_slider');
        $post_image_id = $post_images[0]['news_slider_image'];
        $post_thumb = houston_resize( $post_image_id, 640, 300, true);
        ?>

        <header<?php if ( !$post_thumb ) echo ' class="no-image-post"'; ?>>

            <?php if ( $post_thumb ) : ?>
                <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
                    <img class="post-image" src="<?php echo $post_thumb ?>" alt="<?php the_title() ?>"/>
                </a>
            <?php endif;?>

            <a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
                <h3 class="title"><?php the_title(); ?></h3>
            </a>
            <div class="meta">
                <div class="meta">
                    <i class="icon icon-time"></i><?php the_time('j F Y') ?>
                    <?php $post_categories = get_the_category();
                    if ( $post_categories ) : ?>
                        <i class="icon icon-folder-open"></i>

                        <ul class="cats">
                            <?php $x = 0; foreach ( $post_categories as $cat ) : $x++; ?>
                                <li>
                                    <a href="<?php echo get_category_link( $cat->term_id ) ?>" title="<?php echo $cat->cat_name ?>"><?php echo $cat->cat_name ?></a><?php if ($x != count($post_categories)) echo ', '; ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        </header>

        <p><?php echo excerpt(50); ?></p>

    </article>

<?php endwhile; endif;