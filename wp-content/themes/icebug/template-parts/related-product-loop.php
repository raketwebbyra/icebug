<div class="child-pages">

    <h3><?php echo get_field('related_products_header') ?></h3>
    <?php
        $products = get_field('related_products');
        if(count($products)>0) {
            foreach($products as $product){
                $src = get_field('images_product',$product->ID);
                $src = $src[0]['image'];
                $image = ($src) ? houston_resize($src, 220, false, true) : get_stylesheet_directory_uri().'/images/default.png';
    ?>
                <div class="row">
                    <a class="span4" href="<?php echo get_permalink($product->ID) ?>" alt="<?php echo $product->post_title; ?>" title="<?php echo $product->post_title; ?>">
                        <img src="<?php echo $image; ?>" alt=""/>
                    </a>
                    <div class="span8">
                        <h4 class="title"><?php echo $product->post_title; ?></h4>
                        <p>
                            <?php echo wp_trim_words($product->post_content,25); ?>
                            <a href="<?php echo get_permalink($product->ID) ?>" alt="<?php echo $product->post_title; ?>" title="<?php echo $product->post_title; ?>"><?php _e("Läs mer", "icebug") ?></a>
                        </p>
                    </div>
                </div>
                <?php
            }
        }

    ?>
</div>

