<?php

/* ---------------------------
|
| - Create New Testimonial
|
---------------------------- */

/*

    - Validation

*/

if ( isset( $_POST['submitted'] ) && isset( $_POST['post_nonce_field'] ) && wp_verify_nonce( $_POST['post_nonce_field'], 'post_nonce' ) ) {

    $error_count = 0;
    $error_list = array();

    // Validate product
    if ( trim( $_POST['testimonial_product'] ) === ''  ) {
        $error_list[$error_count] = _x('Du måste välja en produkt', 'Recension Formulär Felmeddelanden', 'icebug');
        $hasError = true;
        $error_count++;
    }

    // Validate name
    if ( trim( $_POST['testimonial_name'] ) === '' ) {
        $error_list[$error_count] = _x('Du måste ange ett namn', 'Recension Formulär Felmeddelanden', 'icebug');
        $hasError = true;
        $error_count++;
    }

    // Validate email
    if ( trim( $_POST['testimonial_email'] ) === '' ) {
        $error_list[$error_count] = _x('Du måste ange en e-postadress', 'Recension Formulär Felmeddelanden', 'icebug');
        $hasError = true;
        $error_count++;
    } elseif ( is_email($_POST['testimonial_email']) == false ) {
        $error_list[$error_count] = _x('Din e-postadress är felaktig', 'Recension Formulär Felmeddelanden', 'icebug');
        $hasError = true;
        $error_count++;
    }

    // Validate location
    if ( trim( $_POST['testimonial_location'] ) === ''  ) {
        $error_list[$error_count] = _x('Du måste välja en ort', 'Recension Formulär Felmeddelanden', 'icebug');
        $hasError = true;
        $error_count++;
    }

    // Validate Rating
    if ( trim( $_POST['testimonial_rating'] ) === ''  ) {
        $error_list[$error_count] = _x('Du måste sätta ett betyg', 'Recension Formulär Felmeddelanden', 'icebug');
        $hasError = true;
        $error_count++;
    }

    // Validate Text
    if ( trim( $_POST['testimonial_text'] ) === ''  ) {
        $error_list[$error_count] = _x('Du måste skriva en recension', 'Recension Formulär Felmeddelanden', 'icebug');
        $hasError = true;
        $error_count++;
    } elseif ( strlen(trim( $_POST['testimonial_rating'] )) > 150 ) {
        $error_list[$error_count] = _x('Recensionen får vara max 150 tecken', 'Recension Formulär Felmeddelanden', 'icebug');
        $hasError = true;
        $error_count++;
    }


    // Validate Language
    if ( trim( $_POST['testimonial_language'] ) === ''  ) {
        $error_list[$error_count] = _x('Du måste ange ett språk', 'Recension Formulär Felmeddelanden', 'icebug');
        $hasError = true;
        $error_count++;
    }

    if ( isset( $_POST['accept_terms'] ) == false ) {
        $error_list[$error_count] = _x('Du måste godkänna att din recension kan komma att användas vid marknadsföringssyfte', 'Recension Formulär Felmeddelanden', 'icebug');
        $hasError = true;
        $error_count++;
    }

    /*

        - Insert New Testimonial into DB

    */

    // If validation passes then create post

    if ( !$hasError ) {

        $product = get_post( $_POST['testimonial_product'] );

        $title = ($_POST['testimonial_product'] != 0) ? $product->post_title : '"Vet ej"';

        $post_information = array(
            'post_title' => 'Recension av ' . $title . ' - ' . $_POST['testimonial_name'],
            'post_content' => 'test',
            'post_type' => 'testemonial',
            'post_status' => 'pending',
        );

        $post_id = wp_insert_post( $post_information );

        // Update custom fields
        update_field('field_5201fad766278', $_POST['testimonial_name'], $post_id); // Update field for name
        update_field('field_5201fb0066279', $_POST['testimonial_email'], $post_id); // Update field for email
        update_field('field_5201fb126627a', $_POST['testimonial_location'], $post_id); // Update field for location
        update_field('field_52024218a2fa9', $_POST['testimonial_rating'], $post_id); // Update field for rating
        update_field('field_5201fb336627b', $_POST['testimonial_text'], $post_id); // Update field for text

        if($_POST['testimonial_product'] != 0){

            $product_images = get_field('images_product', $product->ID);
            $product_image_attachment_id = $product_images[0]['image'];


            update_field('field_5201fa9266277', $_POST['testimonial_product'], $post_id); // Update field for product
            update_field('field_52177baf45572', $product->post_title, $post_id ); // Update field for product title
            update_field('field_52177bb745573', $product_image_attachment_id, $post_id ); // Update field for product image
        }
        update_field('field_5203982d4d3c2', $_POST['testimonial_language'], $post_id); // Update field for language

        // Email the admin with a notification when a new testimonial is created

        $to = array();


	    // This updates the translation table for wordpress post so the testimonial will be created in the current language
	    $translation_query = "UPDATE `{$wpdb->prefix}icl_translations` SET `language_code` = '{$_POST['testimonial_language']}' WHERE `element_id` = {$post_id}";
	    $translation_rows = $wpdb->get_results($translation_query);

        $recipients = get_field('notifcation_email_addresses', 'option');

        foreach ( $recipients as $rep ) {
            $to[] = $rep['notifcation_email_address'];
        }

        $subject = 'En recension väntar godkännande på Icebug.se';

        $message = 'En recension väntar ditt godkännande på Icebug.se' . '<br />';
        $message .= 'Recensionens namn: ' . $title . ' - ' . $_POST['testimonial_name'] . '<br />';
        $message .= '<a href="' . get_home_url() . '/wp-admin/post.php?post=' . $post_id . '&action=edit" title="">Se recensionen här</a>' . '<br />';

        $headers[] = 'From: Icebug.se' . ' <recension@icebug.se>';
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        wp_mail( $to, $subject, $message, $headers);


        //Create session for if post were sent

        if ( !isset( $_SESSION['testimonial_sent'] ) ) {
            $_SESSION['testimonial_sent'] = true;
        }

        if ( $post_id ) {

            $_SESSION['successful'] = 'true';

            // Redirect back to testimonials loop based on current language
            $icl_object_id = icl_object_id(92, 'page', true);
            wp_redirect( get_permalink( $icl_object_id ) );
            exit;
        }

    }

}

?>