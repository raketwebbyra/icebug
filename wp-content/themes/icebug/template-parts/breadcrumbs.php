<section id="breadcrumb-section" class="module">
    <div class="container">
        <div class="row">
            <div class="span12">
                <hr>
                <?php
                if ( is_singular('product') ) {
                    houston_breadcrumbs( true, true );
                } else {
                    houston_breadcrumbs();
                }
                ?>
                <hr>
            </div>
        </div>
    </div>
</section>