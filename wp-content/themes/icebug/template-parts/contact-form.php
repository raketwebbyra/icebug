
<ul class="contact-address">
    <li><strong><?php the_field('contact_name', 'option') ?></strong></li>
    <li><?php the_field('contact_street_address', 'option') ?></li>
    <li><?php the_field('contact_post_address', 'option') ?></li>
    <li><a href="<?php the_field('contact_email', 'option') ?>" title="<?php the_field('contact_email', 'option') ?>"><?php the_field('contact_email', 'option') ?></a></li>
    <li>Tel: <?php the_field('contact_phone', 'option') ?></li>
    <li>Fax: <?php the_field('contact_fax', 'option') ?></li>
</ul>

<h3><?php _ex('Hör av dig till oss', 'Kontaktsidan', 'icebug'); ?></h3>

<div class="page-standard-form contact-form">
<?php gravity_form(1, false, false, false, '', false); ?>
</div>