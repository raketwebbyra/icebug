<?php
    $ads = get_posts(array(
        'post_type' => 'ad',
        'meta_query' => array(
            array(
                'key' => 'show_on_pages_ad', // name of custom field
                'value' => '"'.$post->ID.'"', // matches exaclty "123", not just 123. This prevents a match for "1234"
                'compare' => 'LIKE'
            )
        )
    ));


    if( $ads ){
        foreach($ads as $ad) {
            echo '<a class="ad" href="'.get_field('link_ad',$ad->ID).'" alt="'.$ad->post_title.'" target="'.get_field('target_ad',$ad->ID).'" title="'.$ad->post_title.'">';
                echo '<img src="'.houston_resize(get_field('image_ad',$ad->ID),290,false,true).'" alt="'.$ad->post_title.'"/>';
            echo '</a>';
        }
    }
?>