<div class="child-pages">
    <?php
        $childs = get_pages(array('child_of' => $post->ID, 'sort_column' => 'menu_order'));

        if(count($childs)>0) {
            foreach($childs as $child){
                if($child->post_parent != $post->ID)
                    continue;
                //Else
                    $image = ($src = (get_field('thumbnail_page',$child->ID)) ?: get_field('image_page',$child->ID)) ? houston_resize($src, 160, false, true) : get_stylesheet_directory_uri().'/images/default.png';
                ?>
                <div class="row">
                    <a class="span4" href="<?php echo get_permalink($child->ID) ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                        <img src="<?php echo $image; ?>" alt=""/>
                    </a>
                    <div class="span8">
                        <h4 class="title"><?php echo $child->post_title; ?></h4>
                        <p>
                            <?php echo wp_trim_words($child->post_content,25); ?>
                            <a href="<?php echo get_permalink($child->ID) ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"><?php _e("Läs mer", "icebug") ?></a>
                        </p>
                    </div>
                </div>
                <?php
            }
        }

    ?>
</div>

