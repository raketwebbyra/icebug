<aside id="mobile-sidebar" class="show-for-mobile span12">

            <div id="dl-menu" class="dl-menuwrapper">

                <?php $blog_menu = false;
                if ( is_home() || is_category() || is_singular('post') ) {
                    $blog_menu = true;
                } ?>

                <?php

                if ( !$blog_menu  ) :

                    $ancestors = get_post_ancestors($post->ID);
                    $ancestor = $ancestors[count($ancestors) - 1];?>

                    <button class="dl-trigger"><i class="icon icon-reorder"></i> <?php if ( $post->post_parent == 0 ) : echo get_the_title(); else : echo get_the_title($ancestor); endif; ?></button>

                <?php else : ?>

                    <button class="dl-trigger"><i class="icon icon-reorder"></i> <?php _ex('Kategorier', 'Nyheter', 'icebug') ?></button>

                <?php endif; ?>

                <?php
                if ( !$blog_menu ) {

                    // Output sub-menu
                    echo '<ul class="dl-menu">';
                    wp_list_pages(array(
                        'title_li' => '',
                        'child_of' => ($ancestor = array_pop(get_post_ancestors($post->ID))) ? $ancestor : $post->ID
                    ));
                    echo '</ul>';

                } else {
	                $news_icl = icl_object_id( get_page_by_path('nyheter')->ID , 'page', true);
	                echo '<ul class="dl-menu">';
	                echo '<li><a href="'. get_permalink($news_icl) . '">' . _x('Alla nyheter', 'Nyheter', 'icebug') . '</a></li>';
	                wp_list_categories(array('title_li' => ''));
	                echo '</ul>';
                }

                ?>

            </div>

</aside>