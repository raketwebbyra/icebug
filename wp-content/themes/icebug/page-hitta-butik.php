<?php
/*
 *   Template Name: Hitta din butik
 */
get_header(); ?>

    <!-- Breadcrumbs -->
<?php get_template_part('/template-parts/breadcrumbs'); ?>

    <section id="main-section" class="find-retailer-page-wrap" role="main">

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRi0J-sIAnjUDmigJHLCPUO7RiDYeQFW8&sensor=false"></script>
        <script src="http://www.google.com/jsapi"></script>

        <script type="text/javascript">
            (function ($, window, undefined) {
	            'use strict';

	            /* --------------------------------
	            |
	            |   - Globals
	            |
	            ----------------------------------*/

	            /** Converts numeric degrees to radians */
	            if (typeof(Number.prototype.toRad) === "undefined") {
		            Number.prototype.toRad = function() {
			            return this * Math.PI / 180;
		            }
	            }
                var isMobile = false,
                    isTablet = false,
	                firstInitialize = true;

                if ( Modernizr.mq("screen and (max-width:767px)") ) {
                    isMobile = true;
                }

                if ( Modernizr.mq("screen and (max-width:1023px)") ) {
                    isTablet = true;
                }

                /* --------------------------------
                |
                |   - Create Retailers Array
                |
                ----------------------------------*/

                var retailersArray = [];

                <?php
                $retailers = get_posts(array( 'post_type' => 'retailer', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC' ));
                foreach( $retailers as $retailer ) :
                $lat_lng = get_post_meta($retailer->ID, 'retailer_geolocation');
                $epx_lat = explode('|', $lat_lng[0]);
                $latLongArray = explode(',', $epx_lat[1]);?>

                retailersArray.push({
	                id: <?php echo $retailer->ID ?>,
	                lat: <?php echo $latLongArray[0] ?>,
	                long: <?php echo $latLongArray[1] ?>
                });

                <?php endforeach; ?>


	            /* --------------------------------
	            |
	            |   - Google Maps Init
	            |
	            ----------------------------------*/

                google.maps.visualRefresh = true;

                function initialize() {

                    // Marker image URL
                    var markerImage = '<?php echo get_template_directory_uri() ?>/images/map-marker.png',
	                    mapCenter = new google.maps.LatLng(59.756395,14.626923);

                    var mapOptions = {
                        center: mapCenter,
                        zoom: 5,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        scrollwheel: false,
                        draggable: true,
	                    mapTypeControl: true,
	                    mapTypeControlOptions: {
		                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
		                    position: google.maps.ControlPosition.BOTTOM_CENTER
	                    },
	                    panControl: true,
	                    panControlOptions: {
		                    position: google.maps.ControlPosition.RIGHT_CENTER
	                    },
	                    zoomControl: true,
	                    zoomControlOptions: {
		                    style: google.maps.ZoomControlStyle.LARGE,
		                    position: google.maps.ControlPosition.RIGHT_CENTER
	                    },
	                    scaleControl: true,
	                    streetViewControl: true,
	                    streetViewControlOptions: {
		                    position: google.maps.ControlPosition.RIGHT_CENTER
	                    }
                    };

	                if( isMobile ) {
		                mapOptions['panControl'] = false;
		                mapOptions['zoomControl'] = false;
		                mapOptions['scaleControl'] = false;
		                mapOptions['streetViewControl'] = false;
	                }

                    var map = new google.maps.Map(document.getElementById("map-canvas"),
                        mapOptions);


                    /* --------------------------------
                    |
                    |   - Output Markers
                    |
                    ----------------------------------*/

	                var markers = [];
	                var i = 0;
	                for (var key in retailersArray) {
		                var retailer = retailersArray[key];

		                var retailMarker = new google.maps.Marker({
			                position: new google.maps.LatLng(retailer.lat,retailer.long),
			                map: map,
			                icon: markerImage,
			                itemID: retailer.id
		                });

		                markers[i] = retailMarker;

		                //Event listener for Marker click

		                google.maps.event.addListener(retailMarker, 'click', function() {
			                highlightRetailer( this );
		                });

		                i++;

	                }

					/* --------------------------------
					|
					|   - Highlight Element in Retailers List
					|
					----------------------------------*/

                    function highlightRetailer( marker ) {

	                    var retailersList = $('#retailers-list');

                        map.panTo(marker.getPosition());
                        if ( map.getZoom() < 16 ) {
                            map.setZoom(16);
                        }

                        $('.current-retailer').removeClass('current-retailer');
                        var currentRetailer = $('.retailer-' + marker.itemID);
                        currentRetailer.addClass('current-retailer');
                        $('.current-retailer')
                            .removeClass('inactive-retailer')
                            .siblings().addClass('inactive-retailer');
                        var newTopPos = currentRetailer.position().top + retailersList.scrollTop() - 100;

	                    retailersList.animate({
                            scrollTop : newTopPos
                        }, 700);

	                    if(isMobile) {
		                    $(document).trigger('toggleRetailers');
	                    } else {
		                    $(document).trigger('showRetailers');
	                    }
                    }

                    // Click Retailer

	                $('.retailer').unbind('click');
                    $('.retailer').on('click', function(){
                        var retailerMarker = markers[$(this).data('markerid')];
                        highlightRetailer( retailerMarker );
                    });


                    /* --------------------------------
                    |
                    |   - Search Function
                    |
                    ----------------------------------*/

	                function getRetailersWithinRange(lat, long, range) {

		                var visibleRetailers = [];

		                for(var i = 0; i < retailersArray.length; i++) {

			                var retailer = retailersArray[i],
				                R = 6371;

			                if(lat !== undefined && long !== undefined ) {
				                var dist = Math.floor(Math.acos(Math.sin(lat.toRad()) * Math.sin(retailer.lat.toRad()) + Math.cos(lat.toRad()) * Math.cos(retailer.lat.toRad()) * Math.cos(long.toRad() - retailer.long.toRad())) * R);

				                if ( dist < range ) {
					                retailer.distance = dist;
					                visibleRetailers.push(retailer);
				                }
			                }

		                }

		                // Sort retailers by distance
		                visibleRetailers.sort(function(a,b) {return a.distance - b.distance});
		                return visibleRetailers;
	                }

	                /* --------------------------------
	                |
	                |   - Filter out retailers on search
	                |
	                ----------------------------------*/

	                function checkForNearbyRetailers(lat, long, toggleRetailers)
	                {

		                // Reset List
		                var $retailersWrap = $('.retailers'),
			                $retailersList = $('#retailers-list');

		                $retailersWrap.find('ul')
			                .removeClass('filteredBySearch')
			                .attr('data-order', '')
			                .removeClass('inactive-retailer')
			                .hide();

		                $retailersList.animate({
			                scrollTop: 0
		                });

		                if ( toggleRetailers === true ) {
							// If the retailers list is not visible, show it
			                if(!$retailersList.hasClass('retailers-visible')) $(document).trigger('toggleRetailers');
		                }

		                // Check for retailers within 50 km
		                var visibleRetailers = getRetailersWithinRange(lat, long, 50);
		                // If no retailers was found check for retailers within 100 km
		                if ( visibleRetailers.length < 1 ) visibleRetailers = getRetailersWithinRange(100);

		                // Add the order as a data attribute to the found retailers
		                for ( var i = 0; i < visibleRetailers.length; i++ ) {
			                $retailersWrap.find('ul.retailer-' + visibleRetailers[i].id)
				                .attr('data-order', i)
				                .addClass('filteredBySearch')
				                .show();
		                }

		                var $resultsInfoWrap = $retailersList.find('.results-info-wrap');
		                $resultsInfoWrap.show();

		                if ( visibleRetailers.length > 0 ) {
			                $resultsInfoWrap.find('.results-found').show();
			                $resultsInfoWrap.find('.results-not-found').hide();
			                $resultsInfoWrap.find('.results-info').find('.result-count').text(visibleRetailers.length);

			                if ( isMobile ) {
				                $('.maximize-text').hide();
				                $('.minimize-text').hide();
				                $('.maximize-text-found-retailers')
					                .addClass('active-maximize-btn')
					                .show();
				                $('.retailers-found-count').text(visibleRetailers.length);
			                }

			                var searchTerm = $('.search-map-field').val();

			                if ( !$('.search-map-field').val() ) {
				                searchTerm = $('#you').text();
				                $resultsInfoWrap.find('.term-quote').hide();
			                } else {
				                $resultsInfoWrap.find('.term-quote').show();
			                }

			                $resultsInfoWrap.find('.results-info').find('.result-term').text(searchTerm);

			                // Sort the filtered retailers by their new data attribute
			                $retailersWrap.find('ul.filteredBySearch')
				                .show()
				                .sort(function (a,b) {
					                return $(a).data('order') - $(b).data('order');
				                })
				                .appendTo( $retailersWrap );

		                } else {
			                $resultsInfoWrap.find('.results-found').hide();
			                $resultsInfoWrap.find('.results-not-found')
				                .show()
				                .find('.search-term').text($('.search-map-field').val());
		                }

		                if ( isMobile ) {
			                $('html, body').animate({
				                scrollTop: 0
			                }, 0);
			                $('.search-map-field').blur();
		                }

	                }

	                /* --------------------------------
	                |
	                |   - Geocode Search
	                |
	                ----------------------------------*/

                    var geocoder = new google.maps.Geocoder(),
                        searchField = $('.search-map-field'),
                        errorField = $('.error-msg'),
                        newMarker;

                    function searchMap(address, zoom, toggleRetailers) {

	                    if ( address === undefined ) {
		                    address = searchField.val();
	                    }

	                    if ( zoom === undefined ) {
		                    zoom = 12;
	                    }

	                    if ( toggleRetailers === undefined && isMobile === false ) {
		                    toggleRetailers = true;
	                    }

                        geocoder.geocode(
                            {address : address },

                            function(result, status) {

                                if( status == google.maps.GeocoderStatus.OK ) {

                                    // Reset Marker
                                    if ( newMarker ) {
                                        newMarker.setMap(null);
                                    }

                                    // Fetch location of search result
                                    var loc = result[0].geometry.location,
                                        searchLatLong = new google.maps.LatLng( loc.lat() , loc.lng() );

	                                checkForNearbyRetailers(loc.lat(), loc.lng(), toggleRetailers);

                                    //Pan and Zoom to location
                                    map.panTo(searchLatLong);
                                    map.setZoom(zoom);

                                    // Reset error label
                                    errorField.hide();

                                    // Place new marker
                                    newMarker = new google.maps.Marker({
                                        position: map.getCenter(),
                                        map: map,
                                        title: 'Click to zoom'
                                    });

                                    newMarker.setAnimation(google.maps.Animation.BOUNCE);
                                    setTimeout(function(){ newMarker.setAnimation(null); }, 750);

                                } else {
                                   errorField.show();
                                }
                            }
                        );
                    }

	                /* --------------------------------
	                 |
	                 |   - Mobile auto Detect Position
	                 |
	                 ----------------------------------*/

	                if ( isTablet && firstInitialize == true ) {

		                if (navigator.geolocation) {
			                navigator.geolocation.getCurrentPosition(function(position) {
				                var searchLatLong = new google.maps.LatLng(position.coords.latitude,
					                position.coords.longitude);

				                var toggleRetailers = false,
					                zoom = 13;
				                checkForNearbyRetailers(position.coords.latitude, position.coords.longitude, toggleRetailers);

				                //Pan and Zoom to location
				                map.panTo(searchLatLong);
				                map.setZoom(zoom);

				                // Reset error label
				                errorField.hide();

				                // Place new marker
				                newMarker = new google.maps.Marker({
					                position: map.getCenter(),
					                map: map,
					                title: 'Click to zoom'
				                });

				                newMarker.setAnimation(google.maps.Animation.BOUNCE);
				                setTimeout(function(){ newMarker.setAnimation(null); }, 750);
			                });
		                }

	                }


	                /* --------------------------------
	                 |
	                 |   - Search Field Event
	                 |
	                 ----------------------------------*/

                    $('.search-map-field').on('keyup', function(e) {
                        if ( e.keyCode == 13 ) {
                            searchMap();
                        }
                    });

	                firstInitialize = false;
	                $(document).trigger('map-loaded');

	                return map;

                }



	            /* --------------------------------
	            |
	            |   - Reset Map
	            |
	            ----------------------------------*/

	            function resetSearch() {
		            var $retailersWrap = $('.retailers'),
			            $retailersList = $('#retailers-list'),
			            $showRetailers = $('#show-retailers');

		            $retailersWrap.find('ul')
			            .removeClass('filteredBySearch')
			            .attr('data-order', '')
			            .removeClass('inactive-retailer')
			            .show();

		            $('.retailer').unbind('on');
		            $('.results-info-wrap').hide();
		            initialize();
		            $('.search-map-field').val('');

	            }

	            /* --------------------------------
	            |
	            |   - Document Loaded
	            |
	            ----------------------------------*/

	            $(document).ready(function() {


                    var map = google.maps.event.addDomListener(window, 'load', initialize);

                    $('.retailer a').on('click', function(e) {
                        if ( $(this).parent().parent().hasClass('inactive-retailer') ) {
                            e.preventDefault();
                        }
                    });

		            $(document).on('toggleRetailers', function() {
			            var showRetailersBtn = $('#show-retailers');
			            var retailersList = $('#retailers-list');
			            retailersList.toggleClass('retailers-visible');


			            if ( $('.maximize-text-found-retailers').hasClass('active-maximize-btn') ) {
				            showRetailersBtn.find('.maximize-text-found-retailers').toggle();
			            } else {
				            showRetailersBtn.find('.maximize-text').toggle();
			            }

			            showRetailersBtn.find('.minimize-text').toggle();
		            })

		            $(document).on('showRetailers', function() {
			            var showRetailersBtn = $('#show-retailers'),
			                retailersList = $('#retailers-list');
			            if(retailersList.hasClass('retailers-visible') == false) {
				            retailersList.addClass('retailers-visible');
				            showRetailersBtn.find('.maximize-text').toggle();
			                showRetailersBtn.find('.minimize-text').toggle();
			            }
		            })

	                $('#show-retailers').on('click', function() {
		                $(document).trigger('toggleRetailers');
	                });

		            $('.reset-search').on('click', function(e) {
			            e.preventDefault();
			            resetSearch();
			            if ( isMobile ) {
				            $(document).trigger('toggleRetailers');
			            }
		            })

		            // Calculate Retailers List Height
		            if ( isMobile ) {

			            var retailersList = $('#retailers-list'),
				            newListHeight = $(window).height() - retailersList.offset().top;

			            retailersList.css({
			                height: newListHeight
		                })
		            }

		            $(document).on('map-loaded', function() {
		                if ( firstInitialize == false && isMobile ) {
			                $('.active-maximize-btn').show();
			                $('.maximize-text-found-retailers').hide().removeClass('active-maximize-btn');
		                }
		            });

                });

            })(jQuery, this);

        </script>

        <div id="map-canvas-wrap">

            <div id="map-canvas"></div>

	        <div id="map-tool-bar">

		        <div class="container">
			        <div class="row">

				        <div class="span2 show-btn-wrap">
					        <div id="show-retailers" class="btn expand-btn">
						        <span class="maximize-text">
						            <i class="icon icon-plus-sign"></i> <?php _ex('Lista återföräljare', 'Hitta butik', 'icebug') ?>
						        </span>
						        <span class="maximize-text-found-retailers" style="display: none">
							        <i class="icon icon-plus-sign"></i> <?php _ex('Vi hittade', 'Hitta butik', 'icebug') ?> <span class="retailers-found-count"></span> <?php _ex('återförsäljare nära dig', 'Hitta butik', 'icebug') ?>
						        </span>
						        <span class="minimize-text" style="display: none">
							        <i class="icon icon-minus-sign"></i> <?php _ex('Dölj lista', 'Hitta butik', 'icebug') ?>
						        </span>
					        </div>
				        </div>
				        <div class="span1 map-marker hide-for-tablet">
					        <i class="icon icon-map-marker"></i>
				        </div>
				        <div id="search-field" class="span9">
					        <input type="text" name="search_map_field" class="search-map-field" placeholder="<?php _ex('Skriv din ort...', 'Hitta butik', 'icebug') ?>" />
				        </div>

			        </div>
		        </div><!--//container-->
	        </div>

        </div><!--//map-canvas-wrap-->

        <div id="retailers-list" class="span3">

	        <div class="results-info-wrap" style="display: none">

		        <div class="results-found">
			        <p class="results-info">
				        <?php _ex('Vi hittade', 'Hitta butik', 'icebug') ?>
				        <span class="result-count"></span>
				        <?php _ex('återförsäljare nära', 'Hitta butik', 'icebug') ?>
				        <span class="term-quote">&quot;</span><span class="result-term"></span><span class="term-quote">&quot;</span>
				        <span id="you" style="display: none"><?php _ex('dig', 'Hitta butik', 'icebug') ?></span>
			        </p>
			        <a class="reset-search" href="#"><?php _ex('Återställ sök', 'Hitta butik', 'icebug') ?></a>
		        </div>

		        <div class="results-not-found">
			        <div class="results-info-wrap-no-results">
				        <p><?php _ex('Vi kunde inte hitta några återförsäljare nära', 'Hitta butik', 'icebug') ?> &quot;<span class="search-term"></span>&quot;</p>
				        <a class="reset-search" href="#"><?php _ex('Återställ sök', 'Hitta butik', 'icebug') ?></a>
			        </div>
		        </div>

	        </div>

	        <div class="retailers">

		        <?php
		        $x = 0;
		        global $post;
		        $tmp_post = $post;
		        foreach ( $retailers as $post ) : ?>

			        <ul <?php live_edit('post_title, retail_address, retailer_post_address, retailer_phone, retailer_email, retailer_geolocation') ?> data-markerid="<?php echo $x; ?>" class="retailer retailer-<?php echo $post->ID ?>">
				        <li>
					        <strong class="retailer-title">
						        <?php echo get_the_title( $post->ID ) ?>
					        </strong>
				        </li>
				        <li><span><?php the_field( 'retail_address', $post->ID ) ?></span></li>
				        <li><span><?php the_field( 'retailer_post_address', $post->ID ) ?></span></li>
				        <li><span><?php the_field( 'retailer_phone', $post->ID ) ?></span></li>
				        <li>
					        <a href="mailto:<?php the_field( 'retailer_email', $post->ID ) ?>">
						        <?php the_field( 'retailer_email', $post->ID ) ?>
					        </a>
				        </li>
				        <?php
				        $retailer_web = get_field('retailer_homepage', $post->ID);
				        $retailer_web = str_replace('http://', '', $retailer_web);?>
				        <li>
					        <a target="_blank" href="http://<?php echo $retailer_web ?>" title="Besök <?php echo get_the_title( $post->ID ) ?> på Webben">
						        <?php echo $retailer_web ?>
					        </a>
				        </li>
			        </ul>

			        <?php $x++; endforeach; $post = $tmp_post; ?>

	        </div>

        </div><!--//retailers-list-->


    </section>
<?php get_footer(); ?>