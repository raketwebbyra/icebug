<?php get_header(); ?>

<!-- Breadcrumbs -->
<?php get_template_part('/template-parts/breadcrumbs'); ?>

<!-- Slider -->
<?php get_template_part('/template-parts/single-product-images-slider'); ?>

<?php
    //Check this early for different behaviours later in the context
    $testemonials = get_posts(array(
        'post_type' => 'testemonial',
        'meta_query' => array(
            array(
                'key' => 'testimonial_product',
                'value' => '"'.$post->ID.'"',
                'compare' => 'LIKE'
            )
        )
    ));
?>

<!-- Product info -->
<section id="main-section" class="module border" role="main">
    <div class="container single-product-module">
        <div class="row" <?php live_edit('post_title,post_content,table_product,tech_table_image_product,tech_table_product'); ?>>
            <div class="span8">
                <?php while (have_posts()) : the_post(); ?>
                    <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
                        <?php if($image = get_field('image_page')): ?>
                            <img class="page-image" src="<?php echo houston_resize($image, 530, false, true); ?>" alt="<?php the_title(); ?>"/>
                        <?php endif; ?>

                        <header>

                            <div class="span8">
                                <h1 class="page-title"><?php the_title(); ?></h1>
                                <div class="tax">
                                    <?php
                                    $allowed_taxes = array('product_category');
                                    foreach($allowed_taxes as $tax) {
                                        $terms = wp_get_object_terms($post->ID, $tax);
                                        $terms = array_filter($terms);
                                        foreach($terms as $term){
                                            echo '<a href="' . get_home_url() . '/produkter/?product_category='.$term->slug .'">'. wpml_filter_fix($term->name) .'</a>';
                                        }
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="span4 product-social-btns">

                                <div class="innerSocial">

                                    <?php /* <div class="fb-like" data-href="<?php the_permalink(); ?>" data-width="450" data-layout="button_count" data-show-faces="true" data-send="false"></div> */ ?>
                                    <a href="#"
                                       class="share-btn btn facebook-share-btn"
                                       onclick="
                                        window.open(
                                        'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href),
                                        'facebook-share-dialog',
                                        'width=626,height=436');
                                        return false;">
                                        <i class="icon-facebook-sign"></i>
                                        Dela
                                    </a>

                                    <div class="pinit">
                                        <?php if ( get_field('images_product') ) {
                                            $images = get_field('images_product');
                                            $image = $images[0];
                                            $thumb = houston_resize($image['image'], 500, 500);
                                        } ?>
                                        <a class="share-btn btn pinit-share-btn" target="_blank" href="//pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&description=<?php echo get_the_title() ?>&media=<?php echo $thumb ?>" data-pin-do="buttonPin" data-pin-config="above">
                                            <i class="icon-pinterest-sign"></i>
                                            Pin it!
                                        </a>
                                    </div>

                                    <div class="read-testimonial">
                                        <?php if(count($testemonials) > 0): ?>
                                            <a class="btn scroll" href="#" title="<?php _ex('Se recensioner','Enskild produkt', 'icebug') ?>"><?php _ex('Se recensioner','Enskild produkt', 'icebug') ?> <i class="icon icon-chevron-sign-down"></i></a>
                                        <?php else: ?>
                                            <a class="btn" href="<?php echo get_permalink(icl_object_id( 181, 'page', true )) . '?product-id=' . $post->ID; ?>" title="<?php _ex('Skriv recension','Enskild produkt', 'icebug') ?>"><?php _ex('Skriv recension','Enskild produkt', 'icebug') ?> <i class="icon icon-chevron-sign-down"></i></a>
                                        <?php endif; ?>
                                    </div><!--//read-testimonial-->

                                </div>

                            </div>

                        </header>

                        <?php global $more; $more = 0; ?>


                        <div class="span12">
                            <?php the_content(''); ?>
                        </div>
                    </article>
                <?php endwhile; //End the loop ?>
            </div>

            <div class="span4">
                <div class="product-actions">
                    <a class="btn large" href="<?php echo get_permalink(icl_object_id(254, 'page', true)) ?>" alt="" title=""><?php _ex('Hitta ÅF','Produktsida','icebug') ?> <i class="icon-map-marker"></i></a>
                    <?php if ( ICL_LANGUAGE_CODE != 'en' ) : ?>
                    <a id="product-shop-btn" class="btn orange large" href="<?php echo ($link = get_field('shop_link_product')) ? $link : 'http://shop.icebug.se/shop-i-52.aspx' ?>" alt="" title=""><?php _ex('Shop','Produktsida','icebug') ?> <i class="icon-shopping-cart"></i></a>
                    <?php endif;  ?>
                </div>
                <?php if(get_field('table_product')): ?>
                <table class="product-data-table">
                    <?php while(the_repeater_field('table_product')): ?>
                    <tr>
                        <td><?php echo get_sub_field('key')?></td>
                        <td><?php echo get_sub_field('value')?></td>
                    </tr>
                    <?php endwhile; ?>
                </table>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>


    <!-- Tech information -->
    <section class="module border">
        <div class="container product-tech-module">
            <div class="row">
                <h4 class="module-header span12"><?php _ex('Teknisk info','Produktsida','icebug') ?></h4>
            </div>
            <div class="row">
                <div class="span8">
                    <?php
                    $tech_taxonomies = array(
                        'product_tech_grip',
                        'product_tech_membrane',
                        'product_tech_cold'
                    );
                    foreach($tech_taxonomies as $taxonomy):
                        $terms = wp_get_post_terms($post->ID, $taxonomy);
                        foreach($terms as $term):
                            ?>

                            <div class="row tech">
                                <img class="span2 img" src="<?php echo houston_resize( get_field('icon_tax', $taxonomy . '_' . $term->term_id), 240, false ); ?>" alt=""/>
                                <div class="span10">
                                    <h3 class="title"><?php echo wpml_filter_fix($term->name); ?></h3>

                                    <?php
                                    $readmore = '<a class="more" href="'.get_field('info_page_tax', $taxonomy . '_' . $term->term_id).'" alt="" title="">'._x('Läs mer', 'Produktsida', 'icebug').' <i class="icon-caret-right"></i></a>';
                                    echo wpautop($term->description.' '.$readmore);
                                    ?>
                                </div>
                            </div>
                        <?php endforeach; endforeach; ?>
                </div>

                <div class="span4">
                    <?php
                    if(get_field('tech_table_image_product'))
                        $heatmap_icl_id = icl_object_id(32, 'page', true);
                    echo '<a class="heatmap-image" href="' .  get_permalink($heatmap_icl_id) . '" title="' . get_the_title($heatmap_icl_id) . '">';
                    echo '<img src="'.houston_resize(get_field('tech_table_image_product'),9999,9999).'" alt=""/>';
                    echo '</a>';
                    if(get_field('tech_table_product')):
                        ?>
                        <table class="grid">
                            <thead>
                            <tr>
                                <th></th>
                                <th>1</th>
                                <th>2</th>
                                <th>3</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php while(the_repeater_field('tech_table_product')): ?>
                                <tr>
                                    <td><strong><?php echo get_sub_field('header') ?></strong></td>
                                    <?php $i = 1; foreach(explode("\r\n", get_sub_field('values')) as $value): ?>
                                        <td class="<?php if(in_array($i, get_sub_field('marked_values'))) echo 'marked' ?>"><?php echo $value; ?></td>
                                        <?php $i++; endforeach;  ?>
                                </tr>
                            <?php endwhile; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>



<?php
    // Iterate through the testemonials fetched on row 16
    if(count($testemonials) > 0):
?>
    <!-- Testemonial module -->
    <section class="module secondary border">
        <div class="container testemonial-slider-module">
            <div class="row">
                <div class="span12">
                <div class="testemonial-slider flexslider">

                    <ul class="slides">

                        <?php
                        // Save current product ID in var
                        $current_product_id = $post->ID;
                        // Foreach testimonial
                        foreach ( $testemonials as $testimonial ) :
                            // Setup testimonial as global post object
                            $post = $testimonial;
                            setup_postdata( $post );
                            $product_object = get_field('testimonial_product');
                            $product = $product_object[0];
                            ?>
                            <li class="">

                                <div class="span6">
                                    <div class="quote testimonial-title"><?php echo wpautop('“' . get_field( 'testimonial_text') . '”'); ?></div>
                                    <?php
                                    $rating = intval(get_field('testimonial_rating'));
                                    $max_rating_number = 5;?>

                                    <ul class="testimonial-rating">
                                        <?php for ( $i = 0; $i < 5; $i++ ) { ?>
                                            <li class="rating <?php if( $i < $rating ) echo ' active-rating' ?>">
                                                <span class="icon icon-icebug"></span>
                                            </li>
                                        <?php } ?>
                                    </ul>

                                    <div class="cite testimonial-by">
                                        <?php
                                        $testimonial_name = get_field( 'testimonial_name' );
                                        echo '-  ' . $testimonial_name . '  om <a href="' . get_permalink( $current_product_id ) . '">' . get_the_title($current_product_id) . '</a>'; ?>
                                    </div>
                                    <?php
                                    /* Se all testimonials link
                                    Fetch language friendly post object */
                                    $icl_object_id = icl_object_id(92, 'page', true); ?>
                                    <a class="btn" href="<?php echo get_permalink( $icl_object_id ) ?>" title="<?php _ex('Fler recensioner', 'Enskild produkt', 'icebug') ?>"><?php _ex('Fler recensioner', 'Enskild produkt', 'icebug') ?> <span class="icon icon-chevron-sign-right"></span></a>

                                    <?php
                                    /* Write testimonial about this product link
                                    Fetch language friendly post object */
                                    $write_icl_object_id = icl_object_id( 181, 'page', true );
                                    $write_testimonial = get_permalink($write_icl_object_id) . '?product-id=' . $current_product_id ?>
                                    <a class="btn" href="<?php echo $write_testimonial; ?> " alt="" title="<?php _ex('Skriv recension', 'Enskild produkt', 'icebug') ?>"><?php _ex('Skriv recension', 'Enskild produkt', 'icebug') ?> <span class="icon icon-edit"></span></a>
                                </div><!--//span6-->

                                <?php $product_image_src = get_field('images_product', $product->ID); $product_image_src = $product_image_src[0];
                                if ( $product_image_src ) :
                                    $product_image = houston_resize( $product_image_src['image'], '510', false, false ); ?>

                                    <div class="span6 testimonial-slider-product-image hide-for-mobile">
                                        <img src="<?php echo $product_image ?>" alt="<?php echo $product->post_title ?>"/>
                                    </div>

                                <?php endif; ?>

                            </li>

                        <?php endforeach; wp_reset_postdata(); // end foreach testimonial, reset post data ?>

                    </ul>
                    <script type="text/javascript">
                        jQuery(window).load(function () {
                            jQuery(".testemonial-slider").flexslider({
                                animation: 'fade'
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>


<?php
    /* Get all products which has this product as related */
    $related = get_posts(array(
        'post_type' => 'product',
        'suppress_filters' => 0,
        'meta_query' => array(
            array(
                'key' => 'product_related_to_product', // name of custom field
                'value' => '"'.$post->ID.'"',
                'compare' => 'LIKE'
            )
        )
    ));

    /* Get this product's related products */
    $relationships = get_field('product_related_to_product');
    if ( $relationships ) {

        $x = 0;
        $related_on_products = array();
        foreach ( $relationships as $rel) {
            // If the relationship does not already exist in the related query, then add it
            if ( in_array($rel, $related) == false ) {
                $related_on_products[$x] = $rel;
                $x++;
            }
        }

        /* Merge the arrays */
        $related = array_merge($related_on_products,$related);

    }

    /* If there are related products */
    if(count($related) > 0):

        // Set up deps for rows
        $items_per_row = 3;
        $rows = ceil(count($related)/3);
        if ( is_mobile() ) {
            $items_per_row = 1;
            $rows = count($related);
        }
?>


<h4 class="section-header"><?php _ex('Liknande skor','Produktsida','icebug') ?></h4>
<!-- Related products slider -->
<section class="module secondary border">
    <div class="container product-slider-module">

        <div id="related-product-carousel" class="carousel flexslider">
            <ul class="slides">
                <?php $x = 0; for($i = 0; $i < $rows; $i++ ): ?>
                    <li>
                        <div class="row">
                            <?php
                            for($y = 0; $y < $items_per_row; $y++ ): ?>
                                <div class="span4">
                                    <?php
                                    $src = get_field('images_product',$related[$x]->ID); $src = $src[0]['image'];
                                    $image = ($src) ? houston_resize($src, 300, 200, true) : get_stylesheet_directory_uri().'/images/default.png';
                                    ?>
                                    <a href="<?php echo $related[$x]->guid ?>" title="">
                                        <img src="<?php echo $image; ?>" />
                                    </a>
                                    <h4 class="title"><?php echo $related[$x]->post_title ?></h4>
                                    <p>
                                        <?php echo wp_trim_words($related[$x]->post_content,15); ?>
                                        <a href="<?php echo $related[$x]->guid ?>" alt="" title=""><?php _ex('Läs mer','Produktslider','icebug'); ?> <i class="icon-caret-right"></i></a>
                                    </p>
                                </div>
                                <?php
                                if($x == (count($related)-1)) {
                                    break;
                                }
                                ?>
                            <?php $x++; endfor; ?>
                        </div>
                    </li>
                <?php endfor; ?>
            </ul>
            <script type="text/javascript">
                jQuery(window).load(function () {
                    jQuery("#related-product-carousel").flexslider({
                        animation: 'slide',
                        slideshow: false
                    });
                });
            </script>
        </div>
    </div>

</section>
<?php endif; ?>

<?php get_footer(); ?>


<?php

/* Get all products which has this product as related */
$related = get_posts(array(
    'post_type' => 'page',
    'suppress_filters' => 0,
    'meta_query' => array(
        array(
            'key' => 'projects', // name of custom field
            'value' => '"'.$post->ID.'"',
            'compare' => 'LIKE'
        )
    )
));
?>