<?php
//Set variables for performance
$turl = get_bloginfo("template_url");
//require files for theme INIT
if(!class_exists("lessc")) { require TEMPLATEPATH.'/lib/less/lessc.inc.php'; }
//Compile Less

//try { lessc::ccompile(TEMPLATEPATH.'/css/app.less', TEMPLATEPATH.'/css/app.css'); } catch (exception $ex) { exit('lessc fatal error:<br />'.$ex->getMessage()); }
//try { lessc::ccompile(TEMPLATEPATH.'/css/framework.less', TEMPLATEPATH.'/css/framework.css'); } catch (exception $ex) { exit('lessc fatal error:<br />'.$ex->getMessage()); }
?>
<!doctype html>
<!--[if lt IE 7]> 	<html class="no-js ie6 oldie" lang="en">	<![endif]-->
<!--[if IE 7]>    	<html class="no-js ie7 oldie" lang="en">	<![endif]-->
<!--[if IE 8]>    	<html class="no-js ie8 oldie" lang="en">	<![endif]-->
<!--[if gt IE 8]>	<!--> <html class="no-js" lang="sv"> 		<!--<![endif]-->
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<title><?php wp_title('|', true, 'right'); ?></title>

<!-- Mobile viewport optimized: j.mp/bplateviewport -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta property="og:title" content="<?php wp_title('|', true, 'right'); ?>"/>
<meta property="og:url" content="<?php the_permalink() ?>"/>

<!-- Favicon and Feed -->
<link rel="shortcut icon" type="image/png" href="<?php echo $turl; ?>/favicon.png">
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">
	<script type="text/javascript"></script>
<?php 
	//----------------------------------------------------
	//	Include iOS touch icons if is_mobile()
	//----------------------------------------------------	
	if(function_exists('is_ios') && is_ios()) get_template_part('template-parts/mobile/apple-touch-icons');

	//----------------------------------------------------
	//	Javascriptfiles (Combined & Minified by Houston)
	//----------------------------------------------------
    wp_enqueue_script('jquery');
    wp_enqueue_script('validate', 'http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js');
    wp_enqueue_script('validate_sv', 'http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/localization/messages_sv.js');
    wp_enqueue_script('houston', $turl . '/lib/houston/js/houston.js','','',false);
    wp_enqueue_script('fancybox', $turl . '/lib/fancybox/jquery.fancybox.js','','jquery',false);
    wp_enqueue_script('fancybox-thumbs', $turl . '/lib/fancybox/jquery.fancybox-thumbs.js','','fancybox',false);
    wp_enqueue_script('plugins', $turl . '/js/plugins.min.js','','',true);
	wp_enqueue_script('flexslider', $turl . '/lib/flexslider/jquery.flexslider-min.js','','',true);
	wp_enqueue_script('app', $turl . '/js/app.js','','flexslider',true);

    if ( is_archive() ) {
        wp_enqueue_script('angular', $turl . '/js/vendor/angular.min.js',array('jquery'),'',true);
	    wp_enqueue_script('angular-bootstrap', $turl . '/js/product-filtering/ui-bootstrap-custom-0.10.0.min.js',array('angular', 'jquery'),'',true);
	    wp_enqueue_script('angular-bootstrap-templates', $turl . '/js/product-filtering/ui-bootstrap-custom-tpls-0.10.0.min.js',array('angular', 'jquery'),'',true);
        wp_enqueue_script('product-filtering', $turl . '/js/product-filtering/filtering.js',array('app', 'angular', 'angular-bootstrap', 'angular-bootstrap-templates'),'',true);
    }

	//----------------------------------------------------
	//	Additional CSSfiles & Styles
	//----------------------------------------------------			
    wp_enqueue_style('flexslider', $turl . '/lib/flexslider/flexslider.css');
    wp_enqueue_style('fancybox', $turl . '/lib/fancybox/jquery.fancybox.css');
    wp_enqueue_style('fancybox-thumbs', $turl . '/lib/fancybox/jquery.fancybox-thumbs.css');
    wp_enqueue_style('fontawesome', $turl . '/lib/font-awesome/less/font-awesome.css');


    wp_head();
?>

<!--[if lt IE 9]><script src="<?php echo $turl; ?>/js/vendor/addeventlistener-ie.js"></script><![endif]-->

<link rel="stylesheet" href="<?php echo $turl; ?>/assets/css/framework.css">
<link rel="stylesheet" href="<?php echo $turl; ?>/assets/css/component.css">
<?php $colorTheme = get_field('site_color_theme', 'options') ? get_field('site_color_theme', 'options') : 'winter'; ?>
<link rel="stylesheet" href="<?php echo $turl; ?>/assets/css/app-<?php echo $colorTheme ?>.css">

<?php if ( defined('DEVMODE') == false ) :  ?>
<!-- GA -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-16058544-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
<?php endif; ?>
</head>
<body <?php body_class(); ?>>
<link rel="stylesheet" href="<?php echo $turl; ?>/assets/css/ie.css">
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="<?php echo $turl; ?>/js/vendor/respond.js"></script>
<div class="page-wrap">

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/sv_SE/all.js#xfbml=1&appId=122252307923464";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<!-- Top navigation -->

<div id="mobile-flag" class="show-for-mobile"></div>

<section id="top-section" class="">
    <div class="container">
        <div class="span12 top-slogan">
        <div id="flag" class="hide-for-mobile"></div>
        <?php echo wpautop(_x('Swedish footwear for the new outdoor','Slogan', 'icebug')); ?>

        <form class="pull-right hide-for-mobile" action="<?php bloginfo("home") ?>">
            <input id="search" name="s" type="text"/>
            <label for="search"><i class="icon icon-search"></i></label>
        </form>

        <ul class="top-menu hide-for-mobile list-inline pull-right">
            <?php

            //  List all Top-Navigation links
            wp_nav_menu( array(
                'container' => false,
                'container_class' => false,
                'menu' => 'top-navigation',
                'items_wrap'=>'%3$s'
            ) );

            //  Inject the language selection
            $languages = icl_get_languages('skip_missing=0&orderby=code');
            if(!empty($languages)){
                foreach($languages as $l){

                    if ( ICL_LANGUAGE_CODE != $l['language_code'] ) {

                        $class = ($l['active']) ? 'active ':'';
                        echo '<li class="'.$class.'">';
                        if(!$l['active']) echo '<a href="'.$l['url'].'">';
                        echo $l['native_name'];
                        if(!$l['active']) echo '</a>';
                        echo '</li>';

                    }
                }
            }
            ?>
        </ul>

        </div>
    </div>

</section>

<!-- Navigation -->
<section id="header-section" class="">
    <div class="container">
        <div class="span12 header-section-wrap">

            <div class="menu-btn-logo-wrap">

                <a href="#" class="mobile-show-menu-btn show-for-mobile">
                    <i class="icon icon-reorder"></i>
                </a>
                <a class="pull-left" id="logo" href="<?php bloginfo("home") ?>">
                    <img height="" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt=""/>
                </a>

            </div>

            <div class="nav-wrap hide-for-mobile">

                <?php
                $navigation = 'primary_navigation';
                wp_nav_menu( array(
                    'container' => false,
                    'container_class' => 'navbar-collapse',
                    'theme_location' => $navigation,
                    'menu_class' => 'nav pull-right',
                ) );
                ?>

            </div><!--//nav-wrap-->

            <div id="mobile-menu" style="display:none">

                <?php
                $navigation = 'primary_navigation';
                wp_nav_menu( array(
                    'container' => false,
                    'container_class' => 'navbar-collapse',
                    'theme_location' => $navigation,
                    'menu_class' => 'nav pull-left',
                ) );
                ?>


                <ul class="nav pull-left mobile-meta-links">
                <?php

                //  List all Top-Navigation links
                wp_nav_menu( array(
                    'container_class' => 'navbar-collapse',
                    'menu' => 'top-navigation',
                    'menu_class' => 'nav',
                    'items_wrap'=>'%3$s'
                ) );

                //  Inject the language selection
                $languages = icl_get_languages('skip_missing=0&orderby=code');
                if(!empty($languages)){
                    foreach($languages as $l){

                        if ( ICL_LANGUAGE_CODE != $l['language_code'] ) {

                            $class = ($l['active']) ? 'active ':'';
                            echo '<li class="'.$class.'">';
                            if(!$l['active']) echo '<a href="'.$l['url'].'">';
                            echo $l['native_name'];
                            if(!$l['active']) echo '</a>';
                            echo '</li>';

                        }
                    }
                }
                ?>
                </ul>

                <form class="pull-right mobile-search" action="<?php bloginfo("home") ?>">
                    <input id="search" name="s" type="text"/>
                    <label for="search"><i class="icon icon-search"></i></label>
                </form>

            </div>

        </div>
    </div>
</section>
<!-- End nav -->