'use strict';
module.exports = function(grunt) {

	// load all grunt tasks
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	grunt.initConfig({

		watch: {
			less: {
				files: [
					'assets/less/*.less',
					'assets/less/*/*.less'
				],
				tasks: ['less']
			},
			livereload: {
				options: { livereload: true },
				files: [ 'css/*.css', 'style.css', 'assets/js/*.js', '*.html', '*.php', 'assets/images/**/*.{png,jpg,jpeg,gif,webp,svg}']
			}

		},

		less: {
			development: {
				options: {
					paths: ['css'],
					yuicompress: true,
					syncImport: true
				},
				files: {
					'assets/css/app-summer.css': 'assets/less/app-summer.less',
					'assets/css/app-winter.css': 'assets/less/app-winter.less',
					'assets/css/ui.css': 'assets/less/ui.less',
					'assets/css/framework.css': 'assets/less/framework.less'
				}
			}
		},

		jshint: {
			options: {
				jshintrc: '.jshintrc',
				force: 'true'
			},
			all: [
				'Gruntfile.js',
				'assets/js/source/**/*.js',
				'!assets/js/source/plugins/*',
				'!assets/js/source/plugins.js'
			]
		},

		uglify: {
			plugins: {
				files: {
					'js/plugins.min.js': [
						'js/plugins/**/*.js'
					]
				}
			},
			main: {
				files: {
					'js/app.min.js': [
						'js/app.js'
					],
					'js/product-filtering/filtering.min.js': [
						'js/product-filtering/filtering.js'
					]
				}
			}
		},

		notify: {
			watch: {
				options: {
					title: 'Task Complete',  // optional
					message: 'SASS and Uglify finished running' //required
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-notify');

	// register task
	grunt.registerTask('test', ['jshint']);
	grunt.registerTask('minify', ['jshint', 'uglify']);
	grunt.registerTask('default', ['watch']);


};
