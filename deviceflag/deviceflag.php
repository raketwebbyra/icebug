<?php

class Device_Flag {

    public function Device_Flag($flag = 'device', $mobile = true, $tablet = true, $mobile_param = 'm', $tablet_param = 't') {
        require('mobile-detect.php');
        $detect = new Mobile_Detect();
        $base_url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        if ($mobile) {
            $is_mobile = ($detect->isMobile() && !$detect->isTablet()) ? true : false;
        }
        $is_tablet = ($tablet) ? $detect->isTablet() : false;

        if ( ($is_mobile || $is_tablet) && !isset($_GET[$flag])) {

            $query = parse_url($base_url, PHP_URL_QUERY);
            $device_flag = ( $is_mobile ) ? $mobile_param : $tablet_param ;


            if ( $query ) {
                $base_url .= '&'.$flag.'=' . $device_flag;
            } else {
                $base_url .= '?'.$flag.'=' . $device_flag;
            }

            header("HTTP/1.1 301 Moved Permanently");
            header('Location: http://' . $base_url); exit();
        }

        if ( !$is_mobile && !$is_tablet && isset($_GET[$flag])
            || $is_tablet && $_GET[$flag] == $mobile_param
            || !$is_tablet && $is_mobile && $_GET[$flag] == $tablet_param) {

            $url_params = parse_url($base_url, PHP_URL_QUERY);
            parse_str($url_params, $url_params_array);
            unset($url_params_array[$flag]);

            if ( count($url_params_array) > 0 ) {
                $new_url = $_SERVER['SERVER_NAME'] . strtok($_SERVER["REQUEST_URI"],'?') . '?' . http_build_query($url_params_array);
            } else {
                $new_url = $_SERVER['SERVER_NAME'] . strtok($_SERVER["REQUEST_URI"],'?');
            }
            header("HTTP/1.1 301 Moved Permanently");
            header('Location: http://' . $new_url); exit();
        }
    }

}
